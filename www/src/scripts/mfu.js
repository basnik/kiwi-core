/**
 * Script for multiple file uploader.
 * Uses Fileupload:
 * @see https://blueimp.github.io/jQuery-File-Upload/
 * 
 * @author basnik
 */
;(function($, document, console){
	$(document).ready(function(){
		
		$.nette.ext('kiwi-mfu',{
			init: function(){
				initMfu();
			},
			success: function(){
				initMfu();
			}
		});
		
		function initMfu(){
			$('.jsMfuControl:not(.initialized)').parents('.jsMfuContainer').on('click', '.jsCancelButton', function(){
				$(this).parents('.jsUploadQueueRow').remove();
			});
			
			$('.jsMfuControl:not(.initialized)').each(function(key, item){
				$(item).addClass('initialized');
				
				var $frm = $(item).parents('form');
				$(item).attr('data-submit-url', $frm.find('input[name=_do]').val());

				$(item).fileupload({
					dataType: 'json',
					paramName: $(item).data('paramName') + '[]',
					done: function (e, data) {
						// nette forms hack - restores original parent form target to correctly submit the form
						setFormTargetAsSubmit($frm, $(item));

						$.each(data.files, function (index, file) {
							setSuccessful(hashFileName(file.name), data.result.key);
						});
						fillInUploadedFileNames();
					},
					add: function(e, data){
						
						$.each(data.files, function (index, file) {
							addNewRow(file.name, file.size);
						});
						markLastRow();

						// nette forms hack - sets form target as file upload
						setFormTargetAsUpload($frm, $(item));

						data.submit();
					},
					fail: function(e, data){
						// nette forms hack - restores original parent form target to correctly submit the form
						setFormTargetAsSubmit($frm, $(item));

						setFailed(hashFileName(data.files[0].name));
						if(console){
							console.log('MFU error dump, file:' + data.files[0].name);
							console.log(data.errorThrown);
							console.log(data);
						}
					},
					progress: function (e, data) {
						var progress = parseInt(data.loaded / data.total * 100, 10);
						setProgress(hashFileName(data.files[0].name), progress);
					}
				});
			});
		}
		
		function fillInUploadedFileNames(){
			$('.jsUploadQueueRow:not(.sample)').each(function(key, item){
				var $row = $(item);
				var $outputField = $row.parents('.jsMfuContainer').find('.jsMfuInput');
				var currentFiles = $outputField.val().split(';');
				var key = $row.data('key');
				
				// check if file is not already present
				var regexp = new RegExp('(^|;)?'+key+'($|;)?');
				if(!$outputField.val().match(regexp)){
					currentFiles.push(key);
					$outputField.val(currentFiles.filter(Boolean).join(';'));
				}
			});
		}
		
		function addNewRow(fileName, fileSize){
			
			var $queue = $('.jsUploadQueue');
			var newFileHash = hashFileName(fileName);
			
			var unique = true;
			$queue.find('.jsUploadQueueRow:not(.sample)').each(function(key, item){
				var hash = $(item).attr('id').split('_').pop();
				if(hash === newFileHash){
					unique = false;
				}
			});
			if(unique === false){
				alert('Soubor ' + fileName + ' je již ve frontě pro přidání.\nSoubor nebyl přidán.');
				return;
			}
			
			var $newRow =  $queue.find('.jsUploadQueueRow.sample').clone().removeClass('hide sample');
			
			$newRow.attr('id', 'jsFileRow_' + newFileHash);
			// todo strip long filenames
			$newRow.find('.jsNameCell').text($.kiwi.truncateString(fileName, 50)).attr('title', fileName);
			$newRow.find('.jsSizeCell').text($.kiwi.formatByteSize(fileSize));
			
			$queue.append($newRow);
		}
		
		function setProgress(fileHash, newValue){
			var $fileRow = getFileRow(fileHash);
			$fileRow.find('.jsProgressCell .progress-bar:first')
					.text(newValue + ' %')
					.attr('aria-valuenow', newValue)
					.css('width', newValue+'%');
		}
		
		function setFailed(fileHash){
			var $fileRow = getFileRow(fileHash);
			$fileRow.find('.jsProgressCell .progress-bar:first').addClass('progress-bar-danger');
		}
		
		function setSuccessful(fileHash, key){
			var $fileRow = getFileRow(fileHash);
			$fileRow.find('.jsProgressCell .progress-bar:first').addClass('progress-bar-success');
			$fileRow.data('key', key);
		}
		
		function markLastRow(){
			$('.jsUploadQueueRow').removeClass('last');
			$('.jsUploadQueueRow:last').addClass('last');
		}
		
		function hashFileName(fileName){
			return fileName.replace(/\W+/g, '').toLowerCase().substr(0, 20) + '-' + hashString(fileName);
		}
		
		function getFileRow(fileHash){
			return $('#jsFileRow_' + fileHash.replace('.', '\\.'));
		}
		
		function hashString(string){
			var hash = 0, i, chr, len;
			if (string.length == 0){ 
				return hash;
			}
			for (i = 0, len = string.length; i < len; i++) {
			  chr   = string.charCodeAt(i);
			  hash  = ((hash << 5) - hash) + chr;
			  hash |= 0; // Convert to 32bit integer
			}
			return hash;
		}

		function setFormTargetAsUpload($frm, $item) {
			$frm.find('input[name=_do]').val($item.data('url').split("=").pop());
		}

		function setFormTargetAsSubmit($frm, $item) {
			$frm.find('input[name=_do]').val($item.data('submitUrl'));
		}
		
	});
}(jQuery, document, console));
