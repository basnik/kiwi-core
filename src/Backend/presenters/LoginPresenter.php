<?php

namespace Kiwi\Be;

use Nette\Application\UI\Form;
use Nette\Utils\Validators;
use Nette\Utils\DateTime;
use Tomaj\Form\Renderer\BootstrapVerticalRenderer;

/**
 * Description of ContentPresenter
 *
 * @author basnik
 */
class LoginPresenter extends \Kiwi\BasePresenter{
	
	protected $user;
	
	/** @var \Kiwi\Services\UserService */
	protected $users;
	
	/** @var \Kiwi\Authenticator */
	protected $beAuthenticator;

	public function injectUser(\Nette\Security\User $user, \Kiwi\Services\UserService $users, \Kiwi\Authenticator $beAuthenticator){
		$this->user = $user;
		$this->users = $users;
		$this->beAuthenticator = $beAuthenticator;
	}
	
	public function actionLogout(){
		$this->user->logout();
		$this->redirect('Login:');
	}
	
	public function actionPassResetConfirm($hash){
		// kontrola, zda uzivatel existuje a zda pozadal o obnoveni hesla
		$admin = $this->users->getByIdHash($hash);
		if(empty($admin)){
			$this->flashMessage('Neznámý uživatel.', 'danger');
			$this->redirect('Login:');
		}
		if($admin->pwdChangeRequired == NULL || $admin->pwdChangeRequired < new DateTime()){
			$this->flashMessage('Neaktivní nebo propadlá žádost o změnu hesla. Zažádejte znovu.', 'danger');
			$this->redirect('Login:');
		}
		
		$this['passResetConfirmForm']->setDefaults(array(
			'mail' => $admin->email
		));
	}
	
	
	/**
	 * Vytvari komponentu pro formular pro reset hesla
	 * @return Form
	 */
	protected function createComponentPassResetForm(){
		
		$form = new Form();
		$form->setRenderer(new BootstrapVerticalRenderer());
		
		$form->addText('mail', 'E-mail')
				->setRequired('Zadejte e-mail.');
		
		$form->addSubmit('login', 'Odeslat požadavek');
		
		$form->onValidate[] = [$this, "validatePassResetEmail"];
		$form->onSuccess[] = [$this, "passResetSuccess"];
		
		return $form;
	}
	
	public function validatePassResetEmail(Form $form, $values){
		$mail = $values->mail;
		
		if(!Validators::isEmail($mail)){
			$form->addError('E-mail není ve správném tvaru.');
			return;
		}
		
		// kontrola zda takovy ucet existuje
		if(!$this->users->getByEmail($mail)){
			$form->addError('Takový e-mail v systému neexistuje.');
			return;
		}
	}
	
	public function passResetSuccess(Form $form, $values){
		$mail = $values->mail;
		$user = $this->users->getByEmail($mail);
		
		$passChangeTill = $this->sendPassMail($user);
		
		$user->pwdChangeRequired = $passChangeTill;
		$this->users->save($user);

		$this->system->logActivity(sprintf("Uživateli %s (%s, %d) odeslán odkaz na nastavení nového hesla.", $user->email, $user->name, $user->id));
		
		$this->flashMessage(sprintf('Na zadanou adresu byl odeslán e-mail s odkazem pro změnu hesla. Odkaz je platný do %s.', $passChangeTill->format('j.n.Y H:i')), 'success');
		$this->redirect('this');
	}
	
	/**
	 * Vytvari komponentu pro formular pro dokonceni resetu hesla
	 * @return Form
	 */
	protected function createComponentPassResetConfirmForm(){
		$form = new Form();
		$form->setRenderer(new BootstrapVerticalRenderer());
		
		$form->addText('mail', 'E-mail')
				->setRequired('Zadejte e-mail.')
				->getControlPrototype()->readonly = 'readonly';
		
		$form->addPassword('pass', 'Nové heslo')
				->setRequired('Zadejte vaše nové heslo.')
				->setOption('description', 'Heslo musí být alespoň 8 znaků dlouhé a obsahovat zárověň nějakou číslici a zároveň nějaké malé či velké písmeno.')
				->addRule(Form::MIN_LENGTH, 'Heslo musí být alespoň 8 znaků dlouhé', 8)
				->addRule(Form::PATTERN, 'Heslo musí obsahovat číslici', '.*[0-9].*')
				->addRule(Form::PATTERN, 'Heslo musí obsahovat písmeno (malé či velké)', '.*[a-z|A-Z].*')
				->getControlPrototype()->id('jq_passCont');
		$form->addPassword('passCopy', 'Nové heslo znovu')
				->setRequired('Zadejte vaše nové heslo podruhé.')
				->setOption('description', 'Pro jistotu ještě jednou, pro zamezení překlepům.');
		
		$form->addSubmit('login', 'Změnit heslo');
		
		$form->onValidate[] = [$this, "validatePassResetConfirmForm"];
		$form->onSuccess[] = [$this, "passResetConfirmSuccess"];
		
		return $form;
	}
	
	public function validatePassResetConfirmForm(Form $form, $values){
		$mail = $values->mail;
		
		// kontrola shody hesel
		if($values->pass !== $values->passCopy){
			$form->addError('Zadaná hesla nejsou stejná.');
		}
		
		// kontrola zda takovy ucet existuje
		if(!$this->users->getByEmail($mail)){
			$form->addError('Takový e-mail v systému neexistuje.');
			return;
		}
	}
	
	public function passResetConfirmSuccess(Form $form, $values){
		$mail = $values->mail;
		$admin = $this->users->getByEmail($mail);
		
		$admin->pwdChangeRequired = NULL;
		$admin->password = $this->users->hashPassword($values->pass);
		
		$this->users->save($admin);

		$this->flashMessage('Heslo bylo úspěšně změněno. Nyní se můžete přihlásit.', 'success');
		$this->redirect('Login:');
	}
	
	protected function createComponentLoginForm(){
		$form = new Form();
		$form->setRenderer(new BootstrapVerticalRenderer());
		
		$form->addText('email', 'E-mail:', 30, 100)
				->setRequired('Zadejte e-mail.');
		$form->addPassword('password', 'Heslo:', 30, 100)
				->setRequired('Zadejte heslo.');

		$form->addSubmit('login', 'Přihlásit se');
		$form->onSuccess[] = [$this, 'logUserIn'];
		return $form;
	}
	
	
	public function logUserIn(\Nette\Application\UI\Form $form){
		$values = $form->getValues();
		
		try {
			$this->beAuthenticator->login($values->email, $values->password);
			$this->redirect('Profile:');
		} catch (\Nette\Security\AuthenticationException $e) {
			$form->addError('Neplatné uživatelské jméno nebo heslo.');
		}
	}
}
