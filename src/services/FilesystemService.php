<?php

namespace Kiwi\Services;

use Nette\Utils\Image;
use Nette\Utils\FileSystem;
use Kiwi\Entities\File;

/**
 * Description of Kiwi
 *
 * @author basnik
 */
class FilesystemService {

    use \Nette\SmartObject;
	
	const THUMB_QUALITY = 80;
	
	/** @var InternalDirectoryService */
	protected $directories;
	/** @var InternalFileService */
	protected $files;
	
	/** @var string absolute path to file storage */
	protected $storagePath;
	/** @var string absolute path to thumbnail storage */
	protected $thumbPath;
	
	/** @var array List of allowed thumbnail sizes */
	protected $thumbSizes;
	
	
	public function __construct(SystemService $kiwi, InternalFileService $files, InternalDirectoryService $directories){

		$storagePath = $kiwi->getFilesDirectory();
		$thumbPath = $kiwi->getTempDirectory().'/thumbs';
		
		// check storage dir exists
		if(!is_dir($storagePath)){
			throw new \Nette\InvalidStateException('Storage path '.$this->storagePath.' is not a directory.');
		}
		
		$this->storagePath = $storagePath;
		$this->thumbPath = $thumbPath;
		$this->directories = $directories;
		$this->files = $files;
		$this->thumbSizes = $kiwi->getAllowedThumbsizes();
	}
	
	/**
	 * Gets directory entity by given id or NULL of such directory does not exist.
	 * 
	 * @param int $id
	 * @return \Kiwi\Entities\Directory|NULL
	 */
	public function getDirectoryById($id){
		return $this->directories->getById($id);
	}
	
	/**
	 * Gets file entity by given id or NULL of such file does not exist.
	 * 
	 * @param int $id
	 * @return \Kiwi\Entities\File|NULL
	 */
	public function getFileById($id){
		return $this->files->getById($id);
	}
	
	
	/**
	 * Gets content of a directory
	 * Array collection contains two types of entites - File and Directory
	 * @return array
	 */
	public function getDirectoryContent($parentDirId=NULL, $imagesOnly=0, array $allowedContentTypes=array()) {

		$dirCriteria = ["e.parent_id" => $parentDirId];
		$dirOrder = ["e.name" => InternalDirectoryService::ASC];
		
		$directories = $this->directories->getAllMatching($dirCriteria, $dirOrder);

		$fileCriteria = ["e.directory_id" => $parentDirId];
		if ($imagesOnly != 0) {
			$fileCriteria[] = ["e.is_image = %i", 1];
		}
		if (count($allowedContentTypes) > 0) {
			$fileCriteria[] = ["e.content_type IN %in", $allowedContentTypes];
		}
		
		$fileOrder= ["e.name" => InternalFileService::ASC];
		
		$files = $this->files->getAllMatching($fileCriteria, $fileOrder);
		
		return array_merge($directories, $files);
	}
	
	/**
	 * Saves a directory.
	 * 
	 * @param \Kiwi\Entities\Directory $directory Directory to be saved.
	 * @throws \Kiwi\DuplicateException
	 * @return void
	 */
	public function saveDirectory(\Kiwi\Entities\Directory $directory){
		
		$this->checkUnique($directory);
		
		$this->directories->save($directory);
	}
	
	/**
	 * Saves a file - only to database, provides no operations with physical files.
	 * 
	 * @param \Kiwi\Entities\Directory $file File to be saved.
	 * @throws \Kiwi\DuplicateException
	 * @return void
	 */
	public function saveFile(\Kiwi\Entities\File $file){
		
		$this->checkUnique($file);
		
		$this->files->save($file);
	}
	
	/**
	 * Stores a file in filesystem.
	 * 
	 * @param int $dirId Target directory, where the file is about to be stored
	 * @param string $path Absolute (from root) path to file which will be copied to filesystem.
	 * @param int $ownerId Id of user owning the file.
	 * @param string $fileName Name of the file in filesystem. Optional, if not provided, name detected from path will be used.
	 * @param int $size Size of the file. Optional, will be counted if not provided.
	 * @return File New file entity
	 * 
	 * @throws \Nette\InvalidArgumentException
	 * @throws \Kiwi\DuplicateException
	 */
	public function addFile($dirId, $path, $ownerId, $fileName=NULL, $size=NULL){
		
		// check file exists
		if(!file_exists($path)){
			throw new \Nette\InvalidArgumentException('File ('.$path.') not found.');
		}
		
		// first get directory
		$directory = NULL;
		if($dirId !== NULL){
			$directory = $this->directories->getById($dirId);
			if(!$directory){
				throw new \Nette\InvalidArgumentException('Directory ('.$dirId.') not found.');
			}
		}
		
		// normalize and prepare data
		$fileName = $fileName === NULL ? basename($path) : $fileName;
		$size = $size === NULL ? filesize($path) : $size;
		$contentType = finfo_file(finfo_open(FILEINFO_MIME_TYPE), $path);
		$storageName = uniqid(date('y_n_j_H_i'), true);
		
		// create file entity
		$file = new \Kiwi\Entities\File();
		$file->directoryId = $dirId;
		$file->ownerId = $ownerId;
		$file->name = $fileName;
		$file->size = $size;
		$file->contentType = $contentType;
		$file->storageName = $storageName;
		$file->isImage = substr($contentType, 0, 5) == 'image';
		$file->created = new \DateTime();
		
		// check that file does not already exist in target directory
		$this->checkUnique($file);
		
		// move file to storage
		\Nette\Utils\FileSystem::rename($path, $this->storagePath.'/'.$storageName, TRUE);
		
		// store file in database
		return $this->files->save($file);
	}
	
	/**
	 * Deletes given file
	 * 
	 * @param \Kiwi\Entities\File $file
	 */
	public function deleteFile(\Kiwi\Entities\File $file){
		
		// delete file from storage
		@unlink($this->storagePath.'/'.$file->storageName);
		
		// delete file from database
		$this->files->deleteById($file->id);
	}
	
	/**
	 * Deletes given directory
	 * 
	 * @param \Kiwi\Entities\Directory $directory
	 */
	public function deleteDirectory(\Kiwi\Entities\Directory $directory){

		$children = $this->directories->getAllMatching(["parent_id" => $directory->id]);
		foreach($children as $childDir){
			$this->deleteDirectory($childDir);
		}

		$files = $this->files->getAllMatching(["directory_id" => $directory->id]);
		foreach($files as $file){
			$this->deleteFile($file);
		}
		
		// delete directory from database
		$this->directories->deleteById($directory->id);
	}
	
	/**
	 * Returns overall size of a directory
	 * 
	 * @param \Kiwi\Entities\Directory $dir Target directory to count size of
	 * @return int Size of directory
	 */
	public function getSizeOf(\Kiwi\Entities\Directory $dir){
		
		$dirSize = 0;

		$children = $this->directories->getAllMatching(["parent_id" => $dir->id]);
		foreach($children as $childDir){
			$dirSize += $this->getSizeOf($childDir);
		}
		
		$files = $this->files->getAllMatching(["directory_id" => $dir->id]);
		foreach($files as $file){
			$dirSize += $file->size;
		}

		return $dirSize;
	}
	
	/**
	 * Checks if file or direcory name is unique in its directory.
	 * 
	 * @param \Kiwi\Entities\Directory | \Kiwi\Entities\File $entity
	 * @return void
	 * @throws \Kiwi\DuplicateException
	 */
	public function checkUnique($entity){

		$parentDir = NULL;
		if($entity instanceof \Kiwi\Entities\Directory){
			$parentDir = $entity->parentId;
		}else if($entity instanceof \Kiwi\Entities\File){
			$parentDir = $entity->directoryId;
		}

		$dirCount = $this->directories->countAllMatching([
			'parent_id' => $parentDir,
			'name' => $entity->name
		]);

		$fileCount = $this->files->countAllMatching([
			'directory_id' => $parentDir,
			'name' => $entity->name
		]);
		
		if($dirCount > 0 || $fileCount > 0){
			throw new \Kiwi\DuplicateException('File/Dir name not unique ('.$entity->name.')', 69, NULL, $entity->name);
		}
	}
	
	/**
	 * Returns allowed thumb sizes as array of arrays. Array has keys w, h and b. w defines allowed thubnail widths, h thumbnail heights and b defines both dimensions.
	 * 
	 * @return array
	 */
	public function getAllowedThumbnailSizes(){
		return $this->thumbSizes;
	}
	
	/**
	 * Returns full path on server to a given file.
	 * If given file is not image, width and height parameters are ignored.
	 * 
	 * 
	 * @param File $file
	 * @param int $width
	 * @param int $height
	 * @return string
	 * @throws \Nette\FileNotFoundException
	 */
	public function getFullFilePath(File $file, $width=0, $height=0){
		if(empty($file)){
			throw new \Nette\FileNotFoundException('No such file.');
		}
		
		$filePath = $this->storagePath.'/'.$file->storageName;
		$finalPath = '';
		
		if(!$file->isImage || ($width == 0 && $height == 0)){
			// serving original
			$finalPath = $filePath;
			
		}else if($width == 0){
			// serving height defined miniature
			$this->checkImageThumbDir();
			
			$image = Image::fromFile($filePath);
			$image->resize(NULL, $height, Image::SHRINK_ONLY);
			$image->sharpen();
			
			$finalPath = $this->thumbPath.'/'.$file->id.'_h'.$height.'.jpg';
			$image->save($finalPath, self::THUMB_QUALITY, Image::JPEG);
			
		}else if($height == 0){
			// serving width defined miniature
			$this->checkImageThumbDir();
			
			$image = Image::fromFile($filePath);
			$image->resize($width, NULL, Image::SHRINK_ONLY);
			$image->sharpen();
			
			$finalPath = $this->thumbPath.'/'.$file->id.'_w'.$width.'.jpg';
			$image->save($finalPath, self::THUMB_QUALITY, Image::JPEG);
			
		}else{
			// serving 'fit to dimensions' miniature
			$this->checkImageThumbDir();
			
			$image = Image::fromFile($filePath);
			$image->resize($width, $height, Image::SHRINK_ONLY);
			$image->sharpen();
			
			$finalPath = $this->thumbPath.'/'.$file->id.'_b'.$width.'x'.$height.'.jpg';
			$image->save($finalPath, self::THUMB_QUALITY, Image::JPEG);
		}
		
		return $finalPath;
	}
	
	public function getFullFilePathById($id, $width=0, $height=0){
		
		return $this->getFullFilePath($this->getFileById($id), $width, $height);
		
	}
	
	private function checkImageThumbDir(){
		if(!is_dir($this->thumbPath)){
			FileSystem::createDir($this->thumbPath, 0777);
		}
	}

}
