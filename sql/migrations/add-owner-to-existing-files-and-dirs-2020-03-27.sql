-- alters to add the owner columns
ALTER TABLE `kw_files`
ADD `owner_id` int(11) NULL AFTER `directory_id`,
ADD FOREIGN KEY (`owner_id`) REFERENCES `kw_users` (`id`) ON DELETE RESTRICT;

ALTER TABLE `kw_directories`
ADD `owner_id` int(11) NULL AFTER `parent_id`,
ADD FOREIGN KEY (`owner_id`) REFERENCES `kw_users` (`id`) ON DELETE RESTRICT;

-- pick one user from kw_users table and set his id instead of <id> placeholder
UPDATE kw_files set owner_id=<id>;
UPDATE kw_directories set owner_id=<id>;
