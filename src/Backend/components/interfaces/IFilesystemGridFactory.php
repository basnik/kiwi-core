<?php
namespace Kiwi\Be;

/**
 * Used for DI.
 * 
 * @internal
 * @author basnik
 */
interface IFilesystemGridFactory {
	
	/** @return \Kiwi\Be\FilesystemGrid */
	public function create();
}
