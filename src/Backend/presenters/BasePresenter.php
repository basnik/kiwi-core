<?php

namespace Kiwi\Be;

use Nette\ComponentModel\IComponent;
use Nette\UnexpectedValueException;
use Nette\Utils\Strings;

/**
 * Parent presenter for all backend presenters used in kiwi-based application.
 *
 * @author basnik
 */
abstract class BasePresenter extends \Kiwi\BasePresenter {

	protected $noProfile;

	public function startup(){
		parent::startup();
		
		// redirect unlogged user to login
		if(!$this->user->isLoggedIn()){
			$this->forward(':Kiwi:Be:Login:');
		}
		
		// always invalidate flashes
		if ($this->isAjax()) {
			$this->redrawControl('flashes');
		}

		// register body classes to template
		$this->template->kiwiBodyClasses = array();
	}

	public function beforeRender() {
		parent::beforeRender();

		$this->template->baseLayout = __DIR__.'/../templates/@layout.latte';

		// todo check file exists
		$mainBeLayout = $this->system->getMainBeLayoutPath();
		if ($this->system->getMainBeLayoutPath() != null) {
			$this->template->mainBeLayout = $this->system->getAppDirectory().'/../'.$mainBeLayout;
		} else {
			$this->template->mainBeLayout = '@layout.latte'; // top level layout defined in Kiwi
		}

		$layoutControl = $this->system->getMainBeLayoutControl();
		if ($layoutControl != null) {
			$layoutControl->beforeRender($this);
		}

		$noProfileRoles = $this->system->getNoProfileForRoles();
		$matchedRoles = array_intersect($noProfileRoles, $this->user->getRoles());
		$this->noProfile = count($matchedRoles) > 0;
		$this->template->noProfile = $this->noProfile;
	}


	protected function createComponent(string $name): ?IComponent {

		$layoutControl = $this->system->getMainBeLayoutControl();
		if ($layoutControl != null) {
			$ucname = ucfirst($name);
			$method = 'createComponent' . $ucname;
			if ($ucname !== $name && method_exists($layoutControl, $method) /*&& $layoutControl->getReflection()->getMethod($method)->getName() === $method*/) {
				$component = $layoutControl->$method($name);
				if (!$component instanceof IComponent && !isset($this->components[$name])) {
					$class = get_class($this);
					throw new UnexpectedValueException("Method $class::$method() did not return or create the desired component.");
				}
				return $component;
			}
		}

		// control not found in service, proceed as normal
		return parent::createComponent($name);
	}
}
