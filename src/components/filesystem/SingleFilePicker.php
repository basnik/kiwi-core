<?php
namespace Kiwi;

use Nette\Utils\Html;

/**
 * Selects one file from filesystem, returning its id.
 * 
 * @author basnik
 */
class SingleFilePicker extends BaseFilesystemSinglePicker{
	
	/**
	 * Gets control.
	 * 
	 * @return DatePicker
	 */
	public function getControl(): \Nette\Utils\Html {
		
		$parent = Html::el('div', array( 'class' => 'jsKiwiFileChooserCont'));
		
		$control = parent::getControl();
		$control->class('jsKiwiFileChooserInput hidden', TRUE);
		
		if($this->isDisabled() || $this->getControlPrototype()->readonly){
			$button = Html::el('button', array('class' => 'btn btn-default', 'type' => 'button', 'disabled' => 'disabled'))->setText('Vyberte soubor');
		}else{
			$button = Html::el('button', array( 'class' => 'jsKiwiFileChooserTrigger btn btn-default', 'type' => 'button'))
				->setText('Vyberte soubor');
		}

		$queue = Html::el('div', array( 'class' => 'jsKiwiFileChooserQueue kiwiFChQueue clearfix'));
		$queue->addHtml($this->getQueueFileItem()); // sample for creating new files
		if(!empty($this->value)){
			$queue->addHtml($this->getQueueFileItem($this->value));
		}
		
		$parent->addHtml($control);
		$parent->addHtml($button);
		$parent->addHtml($queue);
		
		return $parent;
	}

}
