<?php
namespace Kiwi\Be;

/**
 * Used for DI.
 */
interface IBackendLayoutService {

	public function beforeRender($presenter);
}