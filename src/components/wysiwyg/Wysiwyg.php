<?php
namespace Kiwi;

/**
 * Adds wysiwyg editor summernote.
 *
 * @see http://summernote.org/
 * @author basnik
 */
class Wysiwyg extends \Nette\Forms\Controls\TextArea{
	
	/** @var int Only basic icons like bold, underline ... */
	const TOOLBAR_BASIC = 1;
	
	/** @var int More possibilities like tables, lists, images ... */
	const TOOLBAR_ADVANCED = 5;
	
	
	/** @var int Editor precise width */
	protected $width;
	
	/** @var int Editor initial height. If it is not disabled, resize handle will be shown to the user. */
	protected $height;
	
	/** @var int Editor minimal height. Takes effect only if editor height is set. No effect if disableResize is set to true. */
	protected $minHeight;
	
	/** @var int Editor maximal height. Takes effect only if editor height is set. No effect if disableResize is set to true. */
	protected $maxHeight;
	
	/** @var boolean Whether to disable editor resizing (true = disabled, false, enabled) */
	protected $disableResize;
	
	/** @var int Toolbar type. One of this class constants (TOOLBAR_BASIC, TOOLBAR_ADVANCED). */
	protected $toolbar = self::TOOLBAR_BASIC;
	
	/** @var boolean Enable fullscreen icon in toolbar */
	protected $enableFullscreen;
	
	/** @var boolean Enable html code view with codemirror */
	protected $showCodeView;


	/**
	 * Sets precise editor width
	 * 
	 * @param int $width
	 * @return \Kiwi\Wysiwyg
	 */
	public function setWidth($width) {
		$this->width = $width;
		return $this;
	}

	/**
	 * Sets initial editor height.  If it is not disabled, resize handle will be shown to the user.
	 * 
	 * @param int $height
	 * @return \Kiwi\Wysiwyg
	 */
	public function setHeight($height) {
		$this->height = $height;
		return $this;
	}

	/**
	 * Sets minimal editor height. Takes effect only if editor height is set. No effect if disableResize is set to true.
	 * 
	 * @param int $minHeight
	 * @return \Kiwi\Wysiwyg
	 */
	public function setMinHeight($minHeight) {
		$this->minHeight = $minHeight;
		return $this;
	}

	/**
	 * Sets maximal editor height. Takes effect only if editor height is set. No effect if disableResize is set to true.
	 * 
	 * @param int $maxHeight
	 * @return \Kiwi\Wysiwyg
	 */
	public function setMaxHeight($maxHeight) {
		$this->maxHeight = $maxHeight;
		return $this;
	}

	/**
	 * Disables editor resizing.
	 * 
	 * @param boolean $disableResize
	 * @return \Kiwi\Wysiwyg
	 */
	public function disableResize($disableResize=TRUE) {
		$this->disableResize = $disableResize;
		return $this;
	}

	/**
	 * Sets editor toolbar type. One of this class constants (TOOLBAR_BASIC, TOOLBAR_ADVANCED)
	 * 
	 * @param int $toolbar
	 * @return \Kiwi\Wysiwyg
	 */
	public function setToolbar($toolbar) {
		$this->toolbar = $toolbar;
		return $this;
	}

	/**
	 * Enables fullscreen icon in toolbar (disabled by default).
	 * 
	 * @param boolean $enableFullscreen
	 * @return \Kiwi\Wysiwyg
	 */
	public function enableFullscreen($enableFullscreen=TRUE) {
		$this->enableFullscreen = $enableFullscreen;
		return $this;
	}

	/**
	 * Enables code view button with codemirror (disabled by default).
	 * 
	 * @param boolean $showCodeView
	 * @return \Kiwi\Wysiwyg
	 */
	public function showCodeView($showCodeView=TRUE) {
		$this->showCodeView = $showCodeView;
		return $this;
	}

		
	
	public function getControl(): \Nette\Utils\Html {
		
		$summernoteParentDiv = \Nette\Utils\Html::el('div');
		$summernoteDiv = \Nette\Utils\Html::el('div class="jsKiwiTextEditor"');
		
		$control = parent::getControl();
		$control->class('jsKiwiTextEditorTextarea hidden', TRUE);
		
		$summernoteParentDiv->addHtml($summernoteDiv);
		$summernoteParentDiv->addHtml($control);
		
		$value = $this->getValue();
		if(!empty($value)){
			$control->setText('');
			$control->setHtml($value);
		}

		$attrs = array();
		$attrs['data-for'] = $control->attrs['id'];
		
		if(isset($this->width)){
			$attrs['data-wysiwyg-width'] = $this->width;
		}
		if(isset($this->height)){
			$attrs['data-wysiwyg-height'] = $this->height;
		}
		if(isset($this->minHeight)){
			$attrs['data-wysiwyg-min-height'] = $this->minHeight;
		}
		if(isset($this->maxHeight)){
			$attrs['data-wysiwyg-max-height'] = $this->maxHeight;
		}
		if(isset($this->disableResize) && $this->disableResize === TRUE){
			$attrs['data-wysiwyg-no-resize'] = 1;
		}
		if(isset($this->toolbar)){
			switch($this->toolbar){
				case self::TOOLBAR_BASIC:
				case self::TOOLBAR_ADVANCED:
					break;
				default:
					throw new \Nette\InvalidArgumentException('Invalid toolbar type. Use one of the Wysiwyg class constants.');
			}
			
			$attrs['data-wysiwyg-toolbar'] = $this->toolbar;
		}
		if(isset($this->enableFullscreen) && $this->enableFullscreen === TRUE){
			$attrs['data-wysiwyg-enable-full'] = 1;
		}
		if(isset($this->showCodeView) && $this->showCodeView === TRUE){
			$attrs['data-wysiwyg-enable-codeview'] = 1;
		}
		
		$summernoteDiv->addAttributes($attrs);
		
		return $summernoteParentDiv;
	}

}
