<?php
namespace Kiwi\Test;

use \Nette\Security\IAuthenticator;
use \Tester\Assert;

require __DIR__ . '/../../../autoload.php';

\Tester\Environment::setup();



// setup mocks
$user = \Mockery::mock(\Nette\Security\User::class);
$userService = \Mockery::mock(\Kiwi\Services\UserService::class);
$roleService = \Mockery::mock(\Kiwi\Services\RoleService::class);

$authenticator = new \Kiwi\Authenticator($user, $userService, $roleService);

$testEmail = "some@mail.cz";
$testPassword = "123456";
$wrongPassword = "000000";

$testUser = \Kiwi\Entities\User::fromArray([
	"id" => 123,
	"email" => $testEmail,
	"name" => "Test",
	"password" => password_hash($testPassword, PASSWORD_DEFAULT),
	"blocked" => FALSE,
	"pwdChangeRequired" => FALSE,
	"data" => "{}",
	"roles" => "[admin,role1,role2]"
]);

$userService->shouldReceive("getByEmail")
	->with($testEmail)
	->andReturn($testUser);

$user->shouldNotReceive("login");

Assert::exception(function() use($authenticator, $testEmail, $wrongPassword) {
	$authenticator->login($testEmail, $wrongPassword);
}, \Nette\Security\AuthenticationException::class, NULL, IAuthenticator::INVALID_CREDENTIAL);


// teardown mocks
\Mockery::close();



