<?php
namespace Kiwi;

/**
 * Description of WysiwygFilter
 *
 * @author basnik
 */
class WysiwygFilter {

    use \Nette\SmartObject;
	
	/** @var Nette\Application\UI\Presenter Actually used presenter for link creation */
	protected $activePresenter;
	
	
	public function __construct(\Nette\Application\Application $application) {
		$this->activePresenter = $application->getPresenter();
	}
	
	public function __invoke($input) {
		$callback = function($matches){
			
			$link = '';
			$linkPath = '';
			$args = array();
			
			$matchParts = explode(' ', $matches[1]);
			$partCount = count($matchParts);
			if($matchParts[0] == 'Download' && $partCount > 1){
				
				$linkPath = ':Kiwi:File:download';
				$args = array(
					'id' => $matchParts[1]
				);
				
			}else if($matchParts[0] == 'File' && $partCount > 1){
				$linkPath = ':Kiwi:File:get';
				$args = array(
					'id' => $matchParts[1]
				);
				
			}else if($matchParts[0] == 'Image' && $partCount > 1){
				$linkPath = ':Kiwi:File:image';
				$args = array(
					'id' => $matchParts[1]
				);
				
				if($partCount > 2){
					if(substr_count($matchParts[2], 'w') > 0){
						$args['width'] = substr($matchParts[2], 1);
					}else if(substr_count($matchParts[2], 'h') > 0){
						$args['height'] = substr($matchParts[2], 1);
					}else if(substr_count($matchParts[2], 'x') > 0){
						$dims = explode('x', $matchParts[2]);
						$args['width'] = $dims[0];
						$args['height'] = $dims[1];
					}
				}
				
			}
			
			if(!empty($linkPath)){
				$link = $this->activePresenter->link($linkPath, $args);
			}
			
			return '"'.$link.'"';
		};

		$result = preg_replace_callback('/["\']kiwi:\/\/([^"\']*)["\']/i', $callback, $input);
		
		return $result;
	}
	
}
