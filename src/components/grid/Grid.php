<?php
namespace Kiwi;

/**
 * Description of KiwiGrid
 *
 * @author basnik
 */
class Grid extends \Nextras\Datagrid\Datagrid{
	
	/**
	 * 
	 * @param \Nette\ComponentModel\IContainer $parent
	 * @param string $name
	 */
	public function __construct(\Nette\ComponentModel\IContainer $parent = NULL, $name = NULL) {
		$this->addCellsTemplate(__DIR__ . '/../../../../../nextras/datagrid/bootstrap-style/@bootstrap3.datagrid.latte');
	}
	
}
