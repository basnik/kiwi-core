/**
 * Script for kiwi filesystem picker tools.
 * 
 * @author basnik
 */
;(function($, window, document, console){
	

	// namespace check
	if($.kiwi.be.fileSystemPickers){
		throw new 'namespace kiwi.be.fileSystemPickers is already used!';
	}
	
	// define our object
	$(document).ready(function(){
		
		$.kiwi.be.fileSystemPickers = function(){

			var self = {
				init: function(){
					$('.jsKiwiFileChooserInput:not(.ready)').each(function(){

						var $input = $(this);
						var $container = $(this).closest('.jsKiwiFileChooserCont');
						var $button = $container.find('.jsKiwiFileChooserTrigger:first');
						var $queue = $container.find('.jsKiwiFileChooserQueue:first');
						
						var files = [];
						
						// check input value and fill files array if the input is not empty
						var inputVal = $input.val();
						if(inputVal.indexOf(',') !== -1){ // semicolon found in input value - multiple ids are present
							$.each(inputVal.split(','), function(key, fileId){
								files.push(parseInt(fileId));
							});
						}else if(inputVal.length > 0){ // just one file id is present
							files.push(parseInt(inputVal));
						}

						$input.addClass('ready');
						
						// edited items delete event
						$container.find('.jsKiwiFChRemButton:visible').click(function(){
							files = self.onDeleteButtonPress($(this), files);
						});

						$button.click(function(){

							var multiple = $input.data('multiple');
							var onlyimages = $input.data('onlyimages');
							var allowedtypes = $input.data('allowedtypes');
							
							if(!multiple){
								multiple = false;
							}
							if(!onlyimages){
								onlyimages = false;
							}
							if(!allowedtypes){
								allowedtypes = [];
							}
							
							$.kiwi.be.fileSelect({
								imagesOnly: onlyimages,
								multiple: multiple,
								allowedContentTypes: allowedtypes,
								selectedCallback: function(newFiles){
									
									$.each(newFiles, function(key, file){
										
										var $newFile = $queue.find('.jsKiwiFileChooserQueueItemSample:first').clone();
										var $fileLink = $newFile.find('.jsKiwiFchImgLink');
										var $remButton = $newFile.find('.jsKiwiFChRemButton');
										
										$remButton.data('fileid', file.id);
										$newFile.removeClass('jsKiwiFileChooserQueueItemSample hidden');
										$fileLink.attr('href', $fileLink.data('link').replace('--ID--', file.id));
										
										// remove image/file button
										$remButton.click(function(){
											files = self.onDeleteButtonPress($(this), files);
										});
										
										if(onlyimages == true){
											var $thumb = $newFile.find('img');
											$thumb.attr('src', $thumb.data('src').replace('--ID--', file.id));
										}else{
											$fileLink.text(file.name);
										}

										if(multiple == true){
											
											// if file is aleady selected, ignore
											if($.inArray(file.id, files) === -1){ // if item is not in array, -1 is returned, otherwise it return its index
												files.push(file.id);
												$queue.append($newFile);
											}
											
										}else{
											
											// replace currently selected file
											files = [file.id];
											
											$queue.find('.jsKiwiFChItem:not(.hidden)').remove();
											$queue.append($newFile);
										}
										
										$input.val(files.join(','));
									});
								}
							});
						});
					});
					
					
				},
				onDeleteButtonPress: function($button, files){
					var thisFileId = $button.data('fileid');
					files = $.grep(files, function(item){
						return thisFileId !== item;
					});
					
					$button.closest('.jsKiwiFileChooserCont').find('.jsKiwiFileChooserInput').val(files.join(','));
					$button.closest('.jsKiwiFChItem').remove();
					
					return files;
				}
			};

			self.init();

			return self; 
		}();
	});
	
}(jQuery, window, document, console));
