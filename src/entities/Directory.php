<?php

namespace Kiwi\Entities;

use Basnik\Db\Entity;

/**
 * Filesystem directory - just a logical item, they do not exist on server.
 */
class Directory extends Entity {

	/**
	 * Id of parent directory
	 */
	public $parentId = NULL;

	/**
	 * Id of user who owns this directory
	 */
	public $ownerId;

	/**
	 * Name of the owner, readonly.
	 */
	public $ownerName;

	/**
	 * E-mail of the owner, readonly.
	 */
	public $ownerEmail;

	/**
	 * Directory name.
	 */
	public $name;
	
	/**
	 * Size in bytes - additional info only available if set previously
	 * @var int 
	 */
	public $size = NULL;
	
	/**
	 * Directory creation date
	 */
	public $created;


	/**
	 * Converts database row obtained as array to object.
	 * @param array $input
	 * @return User
	 */
	public static function load(array $input) {
		$directory = new Directory();
		$directory->extractKeys($input, "id", "parentId", "ownerId", "name", "created", "ownerName", "ownerEmail");
		return $directory;
	}

	/**
	 * Returns associative array with properties of this object - array should be used to store the model into database.
	 * @return array
	 */
	public function save() : array {
		$result = $this->exportProps( "id", "parentId", "ownerId", "name", "created");
		return $result;
	}
}
