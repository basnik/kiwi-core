<?php
namespace Kiwi\Test;

use \Nette\Security\IAuthenticator;
use \Tester\Assert;

require __DIR__ . '/../../../autoload.php';

\Tester\Environment::setup();



// setup mocks
$user = \Mockery::mock(\Nette\Security\User::class);
$userService = \Mockery::mock(\Kiwi\Services\UserService::class);
$roleService = \Mockery::mock(\Kiwi\Services\RoleService::class);

$authenticator = new \Kiwi\Authenticator($user, $userService, $roleService);

$testEmail = "some@mail.cz";
$testPassword = "123456";

$userService->shouldReceive("getByEmail")
	->with($testEmail)
	->andReturn(NULL);

$user->shouldNotReceive("login");

Assert::exception(function() use($authenticator, $testEmail, $testPassword) {
	$authenticator->login($testEmail, $testPassword);
}, \Nette\Security\AuthenticationException::class, NULL, IAuthenticator::IDENTITY_NOT_FOUND);

// teardown mocks
\Mockery::close();



