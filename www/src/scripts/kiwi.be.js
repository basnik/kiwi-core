/* 
 * Defines basic javascript actions for backend of kiwi
 * 
 * @author basnik
 */
;(function($, document, window){
	
	// namespace check
	if($.kiwi.be){
		throw new 'kiwi.be namespace of jquery is already used!';
	}
	
	// create namespace
	$.kiwi.be = {
		printActualDateTime: function(){
			var date = new Date();
			
			var formatted = date.getDate() + '.';
			formatted += (date.getMonth()+1) + '.';
			formatted += date.getFullYear() + ' ';
			
			formatted += ('0' + (date.getHours())).slice(-2) + ':'; 
			formatted += ('0' + (date.getMinutes())).slice(-2) + ':';
			formatted += ('0' + (date.getSeconds())).slice(-2);

			$('#js_act_time').text(formatted);
		},

		initTooltips: function() {
			// init all uninited tooltips
			$('[data-toggle="tooltip"]:not(.hasTooltip)').tooltip().addClass('hasTooltip');
		}
	};
	
	$(document).on('kiwi.core.loaded', function(){
		
		// bootstrap dropdown
		$('.dropdown-toggle').dropdown();
		
		// modals
		$('.modal.autoshow').modal('show');
		$.nette.ext('kiwi',{
			success: function(){
				// if no modal is open (e.g. because closed by snippet redraw), remove the class from body tag
				if ($(".modal-dialog:visible").length === 0) {
					$("body").removeClass("modal-open");
				}

				$('.modal.autoshow').removeClass('autoshow').modal('show');

				$.kiwi.be.initTooltips();
			}
		});
		
		// kiwi clock
		$.kiwi.be.printActualDateTime();
		window.setInterval(function(){
			$.kiwi.be.printActualDateTime();
		}, 1000);
		
		// flashes
		$('.jsFlashes').on('click', '.jsCloseMsg', function(){
			$(this).parents('.flash').fadeOut('slow');
		});

		// cancel button for forms
		$('body').on('click', '.jsFormCancel', function(){
			if(confirm('Opravdu zrušit?')){
				window.location.href = $(this).data('backurl');
			}
		});
		
		// prompt for defined elements
		$('body').on('click', '.jsPrompt', function(){
			var promptText = $(this).data('prompt');
			if(confirm(promptText) === true){
				return true;
			}
			return false;
		});

		$.kiwi.be.initTooltips();

		// and we are loaded ...
		$(document).trigger('kiwi.be.loaded');
	});
	
})(jQuery, document, window);


