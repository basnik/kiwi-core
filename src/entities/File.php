<?php

namespace Kiwi\Entities;


use Basnik\Db\Entity;

/**
 * File in filesystem. Represents physical file on server disc.
 */
class File extends Entity {

	/**
	 * Id of directory where the file is placed.
	 */
	public $directoryId;

	/**
	 * Id of user who owns this file
	 */
	public $ownerId;

	/**
	 * Name of the owner, readonly.
	 */
	public $ownerName;

	/**
	 * E-mail of the owner, readonly.
	 */
	public $ownerEmail;

	/**
	 * File name as was uploaded by user (original file name). This is not the on-server filename.
	 */
	public $name;

	/**
	 * Physical file name on server. This is the file, where the real data are stored.
	 */
	public $storageName;

	/**
	 * File content type
	 */
	public $contentType;
	
	/**
	 * If file is image
	 */
	public $isImage;

	/**
	 * File size
	 */
	public $size;
	
	/**
	 * File creation date
	 */
	public $created;

	/**
	 * Converts database row obtained as array to object.
	 * @param array $input
	 * @return User
	 */
	public static function load(array $input) {
		$file = new File();
		$file->extractKeys($input, "id", "directoryId", "ownerId", "name",
			"storageName", "contentType", "isImage", "size", "created", "ownerName", "ownerEmail");
		return $file;
	}

	/**
	 * Returns associative array with properties of this object - array should be used to store the model into database.
	 * @return array
	 */
	public function save() : array {
		$result = $this->exportProps( "id", "directoryId", "ownerId", "name",
			"storageName", "contentType", "isImage", "size", "created");
		return $result;
	}
}
