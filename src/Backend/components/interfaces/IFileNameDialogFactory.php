<?php
namespace Kiwi\Be;

/**
 * Used for DI.
 * 
 * @internal
 * @author basnik
 */
interface IFileNameDialogFactory {
	
	/** @return \Kiwi\Be\FileNameDialog */
	public function create();
}
