<?php

namespace Kiwi\Be;

use Nette\Application\BadRequestException;
use Nette\Application\UI\Form;
use Tomaj\Form\Renderer\BootstrapVerticalRenderer;

/**
 * Description of ContentPresenter
 * @todo nefunguje zmena hesla
 *
 * @author basnik
 */
class ProfilePresenter extends BasePresenter{

	/**
	 * @var \Kiwi\Services\UserService
	 */
	protected $users;

	/** @var \Kiwi\Authenticator */
	protected $beAuthenticator;
	
	public function injectUserService(\Kiwi\Services\UserService $users, \Kiwi\Authenticator $beAuthenticator){
		$this->users = $users;
		$this->beAuthenticator = $beAuthenticator;
	}

	public function beforeRender() {
		parent::beforeRender();

		if ($this->noProfile) {
			throw new BadRequestException("Profile not allowed for this user role."); // 404
		}
	}

	protected function createComponentProfileForm(){
			
			$form = new Form();
			$form->setRenderer(new BootstrapVerticalRenderer());

			$form->addText('name', 'Jméno')
				->setRequired('Jméno musí být vyplněné.');
			$form->addText('email', 'E-mail')
				->setRequired('E-mail musí být vyplněn.')
				->addRule($form::EMAIL, 'E-mail musí mít správný formát')
				->addRule([$this, 'validateEmailUniquity'], 'Takový e-mail již existuje.');

			$form->addSubmit('sendData', 'Uložit');
			$form->onSuccess[] = [$this, 'saveProfileForm'];
	
			$user = $this->user->getIdentity();
			$form->setDefaults(array(
				'name' => $user->name,
				'email' => $user->email,
			));
			 
			return $form;
	}
	
	protected function createComponentChangePassForm(){
			
			$form = new Form();
			$form->setRenderer(new BootstrapVerticalRenderer());

			$form->addPassword('oldPass', 'Aktuální heslo')
				->setRequired('Zadejte aktuální heslo.');
				
			$form->addPassword('newPass', 'Nové heslo')
				->setRequired('Zadejte nové heslo.')
				->addRule($form::PATTERN, 'Heslo musí být kombinace malých a velkých písmen a číslic, dlouhá alespoň 8 znaků.', '(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}')
				->setOption('description', 'Heslo musí být kombinace malých a velkých písmen a číslic, dlouhá alespoň 8 znaků.');
				
			$form->addPassword('newPassRe', 'Nové heslo (potvrzení)')
				->setRequired('Zadejte potvrzení nového hesla.')
				->addRule($form::EQUAL, 'Zadaná hesla se musejí shodovat.', $form['newPass']);

			$form->addSubmit('sendData', 'Uložit');
			
			$form->addProtection('Problém při zpracování. Zkuste obnovit stránku.');
			
			$form->onValidate[] = [$this, 'checkOldPass'];
			$form->onSuccess[] = [$this, 'saveChangePassForm'];
			
			return $form;
	}
	
	public function saveProfileForm(\Nette\Application\UI\Form $form){
		$values = $form->getValues();
		
		$userIdentity = $this->user->getIdentity();
		
		$user = $this->users->getById($userIdentity->id);
		$user->name = $values->name;
		$user->email = $values->email;
		
		$this->users->save($user);
		
		// update in current user identity
		$this->user->getIdentity()->name = $values->name;
		$this->user->getIdentity()->email = $values->email;
		
		$this->flashMessage('Změny uloženy.', 'success');
		$this->redirect('this');
	}
	
	public function saveChangePassForm(\Nette\Application\UI\Form $form){
		$values = $form->getValues();
		$userIdentity = $this->user->getIdentity();

		// authenticated by form validation - we can now change the password
		$user = $this->users->getById($userIdentity->id);
		$user->password = $this->users->hashPassword($values['newPass']);
		$this->users->save($user);

		$this->system->logActivity(sprintf("Uživatel %s (%s, %d) - heslo změněno.", $user->email, $user->name, $user->id));
		
		$this->flashMessage('Heslo změněno.', 'success');
		$this->redirect('this');
	}
	
	
	/**
	 *  public to be accessible by callback
	 *  @internal form callback
	 */
	public function validateEmailUniquity(\Nette\Forms\IControl $control){
		$userDb = $this->users->getByEmail($control->getValue());
		$user = $this->user->getIdentity();
		if(!empty($userDb)){
			return $userDb->id == $user->id ? true : false;
		}
		return true;
	}
	
	/**
	 * public to be accessible by callback
	 * @internal form callback
	 * @param \Nette\Forms\Form $form
	 */
	public function checkOldPass(\Nette\Forms\Form $form){
		$values = $form->getValues();
		$user = $this->user->getIdentity();
		
		try {
			$this->beAuthenticator->login($user->email, $values['oldPass']);
			return true;

		} catch (\Nette\Security\AuthenticationException $e) {
			$form->addError('Staré heslo není správné.');
			return false;
		}
	}
	
}
