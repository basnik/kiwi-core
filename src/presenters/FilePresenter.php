<?php

namespace Kiwi;

use Nette\Application\UI\Presenter;
use finfo;
use Nette\Application\Responses\FileResponse;

/**
 * Presenter na stahovani souboru
 *
 * @author basnik
 */
class FilePresenter extends Presenter {

	/** @var Kiwi\Services\FilesystemService */
	protected $fs;
	protected $response;
	
	public function injectServices(\Kiwi\Services\FilesystemService $fs, \Nette\Http\Response $response){
		$this->fs = $fs;
		$this->response = $response;
	}
	
	public function actionDefault($id){
		$this->forward('get', $id);
	}

	public function actionGet($id){
		$fileData = $this->fs->getFileById($id);
		if(empty($fileData)){
			throw new \Nette\Application\BadRequestException('File with such id ('.$id.') not found', 404);
		}

		$filePath = $this->fs->getFullFilePath($fileData);
		if(!file_exists($filePath)){
			throw new \Nette\Application\BadRequestException('File data for file with id ('.$id.') not found', 404);
		}

		$this->response->setHeader('Pragma', 'public');
		$this->response->setHeader('Cache-Control', 'public');

		$this->response->setHeader('Last-Modified', gmdate('D, d M Y H:i:s', filemtime ($filePath)).' GMT');
		$this->response->setHeader('Expires', gmdate('D, d M Y H:i:s', (time()+60*60*24*7)).' GMT');
		
		$fileInfo = new finfo(FILEINFO_MIME);
		$this->response->setHeader('Content-type', $fileInfo->file($filePath));
		$this->response->setHeader('Content-Length', filesize($filePath));
		
		$this->response->setHeader('Content-Disposition', 'inline; filename='.$fileData->name);
		$this->response->setHeader('Content-Transfer-Encoding', 'binary');
		
		$this->serveFile($filePath);
		$this->terminate();
	}
	
	public function actionDownload($id){
		$fileData = $this->fs->getFileById($id);
		if(empty($fileData)){
			throw new \Nette\Application\BadRequestException('File with such id ('.$id.') not found', 404);
		}
		
		$filePath = $this->fs->getFullFilePath($fileData);
		if(!file_exists($filePath)){
			throw new \Nette\Application\BadRequestException('File data for file with id ('.$id.') not found', 404);
		}


		$fileResponse = new FileResponse($filePath, $fileData->name);
		
		$this->response->setHeader('Pragma', 'no-cache');
		$this->response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0');
		$this->response->setHeader('Expires', 0);
		
		$this->response->setHeader('Content-Transfer-Encoding', 'binary');
		$this->response->setHeader('Content-Description', 'File Transfer');
		$this->response->setHeader('Content-Type', 'application/force-download');
		
		$this->sendResponse($fileResponse);
	}
	
	public function actionImage($id, $width='0', $height='0'){
		
		$allowedSizes = $this->fs->getAllowedThumbnailSizes();
		
		if((!is_numeric($width) && !is_null($width) && $width < 10) || (!is_numeric($height) && !is_null($height) && $height < 10) ){
			throw new \Nette\InvalidArgumentException('Image: width and height must be positive numbers greater than 10.');
		}
		
		if(!empty($width) && !empty($height)){
			if(!array_key_exists('b', $allowedSizes)){
				throw new \Kiwi\ThumbsizeNotAllowedException('Image: Unallowed thumb size.');
			}
			if(!in_array($width.'x'.$height, $allowedSizes['b'])){
				throw new \Kiwi\ThumbsizeNotAllowedException('Image: Unallowed thumb size.');
			}
		}else if(!empty($width) && empty($height)){
			if(!array_key_exists('w', $allowedSizes)){
				throw new \Kiwi\ThumbsizeNotAllowedException('Image: Unallowed thumb size.');
			}
			if(!in_array($width, $allowedSizes['w'])){
				throw new \Kiwi\ThumbsizeNotAllowedException('Image: Unallowed thumb size.');
			}
		}else if(empty($width) && !empty($height)){
			if(!array_key_exists('h', $allowedSizes)){
				throw new \Kiwi\ThumbsizeNotAllowedException('Image: Unallowed thumb size.');
			}
			if(!in_array($height, $allowedSizes['h'])){
				throw new \Kiwi\ThumbsizeNotAllowedException('Image: Unallowed thumb size.');
			}
		}
		
		try{
			$fileData = $this->fs->getFileById($id);
			$path = $this->fs->getFullFilePath($fileData, $width, $height);
		}catch (\Nette\FileNotFoundException $e){
			throw new \Nette\Application\BadRequestException('File with such id ('.$id.') not found', 404);
		}catch (\Nette\InvalidArgumentException $e){
			throw new \Nette\Application\BadRequestException('File with id ('.$id.') is not an image', 404);
		}
		
		$this->response->setHeader('Pragma', 'public');
		$this->response->setHeader('Cache-Control', 'public');
		
		$modified = gmdate('D, d M Y H:i:s', (time()-60*60*24)).' GMT';
		$this->response->setHeader('Last-Modified', $modified);
		$this->response->setHeader('Expires', $modified); // todo tady by asi melo byt nejake vyssi datum?
		
		$this->response->setHeader('Content-type', finfo_file(finfo_open(FILEINFO_MIME_TYPE), $path));
		$this->response->setHeader('Content-Length', filesize($path));
		
		$this->response->setHeader('Content-Disposition', 'inline; filename='.$fileData->name);
		$this->response->setHeader('Content-Transfer-Encoding', 'binary');
		
		$this->serveFile($path);
		
		$this->terminate();
	}
	
	
	protected function serveFile($path){
		ob_start();
		
		$handle = fopen($path, 'r');
		while(!feof($handle)){
			// nacteme cast souboru - po 1kB
			echo fread($handle, 1024);
			
			// a odesilame browseru
			ob_flush();
			flush();
			
			// prodlouzime time_limit, aby nahodou behem downloadu nedoslo k jeho naplneni 
			set_time_limit(30);
		}
		fclose($handle);
		
		ob_end_flush();
	}
	
}
