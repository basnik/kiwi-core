<?php
namespace Kiwi;

/**
 * Description of TestAuth
 *
 * @author ales
 */
class TestAuth extends KiwiAuthorizator{
	
	public function buildAuthorizator(\Nette\Security\Permission $auth) {
		
		$this->addRole('panda', 'Panda', 'Panda může jenom okusovat bambus.');
		$this->addRole('fox', 'Liška', 'Liška může okusovat bambus a plenit kurník.', 'panda');
		$this->addRole('ninja', 'Ninja', 'Ninja může okusovat bambus, plenit kurník a sekat na kousky pandy a lišky.', 'fox');
		
		$auth->addResource('file');
		
		$auth->allow('panda', 'file', 'show');
		$auth->allow('fox', 'file', 'edit');
		$auth->allow('ninja', 'file', 'delete');
	}

}
