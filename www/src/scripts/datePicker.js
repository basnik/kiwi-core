/**
 * Custom script to get date picker for kiwi to work.
 * 
 * @author basnik
 */
;(function($, document){
	$(document).ready(function(){

		$('.jsKiwiDatePicker:not(.hasPicker)').each(function(key, item){
			
			$(item).addClass('hasPicker');
			$(item).datepicker({
				language: "cs",
				todayHighlight: true,
				autoclose: true
			});
			
			// click on datepicker icon
			$(item).parents('.jsKiwiDpCont').find('.jsKiwiDpTrigger').click(function(){
				$(item).datepicker('show');
			});
			
		});

	});
})(jQuery, document);