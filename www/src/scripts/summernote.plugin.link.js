/**
 * Script for summernote editor image plugin
 * 
 * @author basnik
 */
;(function ($) {
	
	var selectedFile = null;

	$.extend($.summernote.plugins, {
		
		/**
		 * @param {Object} context - context object has status of editor.
		 */
		'kiwiLink': function (context) {

			// ui has renders to build ui elements.
			var ui = $.summernote.ui;
			var $editor = context.layoutInfo.editor;
			
			// modify link popover
			context.options.popover.link = [
				['link', ['kiwiLink', 'unlink']]
			];
			
			// add link edit button
			context.memo('button.kiwiLink', function () {
				// create button
				var button = ui.button({
					contents: '<i class="note-icon-link"/> ',
					tooltip: 'Vložit nebo upravit odkaz',
					click: function () {
						var $dialogBody = $('#jsLinkDialogBody').clone().removeClass('hidden');
						var $dialogFooter = $('#jsLinkDialogFooter').clone().removeClass('hidden');

						if($dialogBody.length == 0 || $dialogFooter.length == 0){
							throw 'No dialog body and/or footer specified. Unable to open image dialog.';
						}
						
						var text = context.invoke('editor.getSelectedText');
						context.invoke('editor.saveRange');
						
						var dlg = ui.dialog({
							title: 'Vložit nebo upravit odkaz',
							body: $dialogBody.html(),
							footer: $dialogFooter.html()
						});
						
						
						var $dlg = dlg.render();
						$dlg.appendTo('body');
						$dlg.on('shown.bs.modal', function(){
							initDialogContent($dlg, $editor, context);
							context.triggerEvent('dialog.shown');
						});
						
						ui.showDialog($dlg);
					}
				});

				// create jQuery object from button instance.
				return button.render();
			});
		}
	});
	
	function initDialogContent($dialog, $editor, context){
		
		var edit = false;
		
		var linkInfo = context.invoke('getLinkInfo');
		var $fileUrlRow = $dialog.find('.jsFileUrlRow');
		var $externalUrlRow = $dialog.find('.jsExternalUrlRow');
		
		// detekce zda nebyl vybran nejaky prvek
		if(linkInfo.range.ec.nodeName.toLowerCase() == 'a'){
			selectedFile = null;
			edit = true;
			
			var type = $(linkInfo.range.ec).data('type');
			
			$dialog.find('.jsSourceToggle input[type=radio][value='+type+']').parents('label').click();
			$dialog.find('.jsText').val(linkInfo.text);
			if(type == 'external'){
				$fileUrlRow.addClass('hidden');
				$externalUrlRow.removeClass('hidden');
				
				$dialog.find('.jsExternalUrl').val(linkInfo.url);
				
				
			}else{
				
				$externalUrlRow.addClass('hidden');
				$fileUrlRow.removeClass('hidden');
				
				$dialog.find('.jsFileUrl').val(linkInfo.url);
				
				var fileId = linkInfo.url.split(' ').pop();
				selectedFile = fileId;
				
				// load file path
				var loadPath = $('#jsFilePathLink').val().replace('--id--', fileId);
				$.get(loadPath, function(response){
					$fileUrlRow.find('.jsSelectedFileName').removeClass('hidden').text($.kiwi.truncateString(response.path, 75)).attr('title', response.path);
				});
			}
			
			$dialog.find('.jsTarget').prop('checked', linkInfo.isNewWindow);
		}else{
			$dialog.find('.jsSourceToggle:first').click();
			$dialog.find('.jsText').val('');
			$dialog.find('.jsExternalUrl').val('');
			$dialog.find('.jsFileUrl').val('');
			$dialog.find('.jsTarget').prop('checked', false);
		}
	
		if($dialog.hasClass('initComplete')){
			return;
		}

		// external / internal url toggle
		$dialog.find('.jsSourceToggle').click(function(){
			if($(this).find('input[type=radio]').val() == 'file'){
				
				$externalUrlRow.addClass('hidden');
				$fileUrlRow.removeClass('hidden');
			}else{
				
				$fileUrlRow.addClass('hidden');
				$externalUrlRow.removeClass('hidden');
			}
		});
		
		// file select for internal link
		$fileUrlRow.find('.jsFileselectLauncher').click(function(){
			$.kiwi.be.fileSelect({
				selectedCallback: function(files){
					selectedFile = files.pop();

					$fileUrlRow.find('.jsFileUrl').val('kiwi://Download '+selectedFile.id);
					$fileUrlRow.find('.jsSelectedFileName').removeClass('hidden').text($.kiwi.truncateString(selectedFile.path, 75)).attr('title', selectedFile.path);
				}
			});
		});
		
		// dialog button actions
		$dialog.find('.jsImageClose').click(function(){
			$dialog.modal('hide');
		});
		$dialog.find('.jsImageInsert').click(function(){
			insertFinalLink($dialog, $editor, context, edit, linkInfo.range);
		});
		
		$dialog.addClass('initComplete');
	}
	
	function insertFinalLink($dialog, $editor, context, edit, range){
		
		var text = $dialog.find('.jsText').val();
		var url = '';
		var newWindow = false;
		var type = 'external';

		if($dialog.find('.jsSourceToggle input[type=radio]').filter(':checked').val() == 'external'){

			url = $dialog.find('.jsExternalUrl').val();

		}else{

			url = $dialog.find('.jsFileUrl').val();
			type = 'file';

		}
		
		if(url.length == 0){
			alert('Vyplňte URL.');
			return;
		}
		
		if($dialog.find('.jsTarget').is(':checked')){
			newWindow = true;
		}

		var attrs = {
			'href': url,
			'data-type': type
		};
		
		// new window open
		if(newWindow == true){
			attrs.target = '_blank';
		}
		
		

		var $aEl = $('<a></a>');
		$aEl.attr(attrs);
		$aEl.text(text);

		context.invoke('editor.restoreRange');
		if(edit == true){
			range.deleteContents();
		}
		
		context.invoke('editor.insertNode', $aEl.get(0));

		$dialog.modal('hide');
	}

}(jQuery));

