<?php
namespace Kiwi\Test;

use \Tester\Assert;

require __DIR__ . '/../../../autoload.php';

\Tester\Environment::setup();



// setup mocks
$user = \Mockery::mock(\Nette\Security\User::class);
$userService = \Mockery::mock(\Kiwi\Services\UserService::class);
$roleService = \Mockery::mock(\Kiwi\Services\RoleService::class);

$authenticator = new \Kiwi\Authenticator($user, $userService, $roleService);

$testEmail = "some@mail.cz";
$testPassword = "123456";
$testRoles = ["admin", "user"];

$testUser = \Kiwi\Entities\User::fromArray([
	"id" => 123,
	"email" => $testEmail,
	"name" => "Test",
	"password" => password_hash($testPassword, PASSWORD_DEFAULT),
	"blocked" => FALSE,
	"pwdChangeRequired" => FALSE,
	"data" => "{}",
	"roles" => json_encode($testRoles)
]);

$testUserData = [
	'id' => $testUser->id,
	'name' => $testUser->name,
	'email' => $testUser->email,
	'data' => $testUser->data
];

$testIdentity = new \Kiwi\Identity(
	$testUser->id,
	$authenticator,
	$testUser->roles,
	$testUserData
);

$userService->shouldReceive("getByEmail")
	->with($testEmail)
	->andReturn($testUser);

foreach($testUser->roles as $role) {
	$roleService->shouldReceive("roleExists")
		->with($role)
		->andReturn(TRUE);
}

$user->shouldReceive("login")
	->with(\Mockery::on(function($arg) use($testIdentity) {
		Assert::equal($testIdentity, $arg);
		return $testIdentity == $arg;
	}))
	->once();

$authenticator->login($testEmail, $testPassword);

// teardown mocks
\Mockery::close();



