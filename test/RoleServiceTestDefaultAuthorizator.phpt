<?php
namespace Kiwi\Test;

use \Tester\Assert;

require __DIR__ . '/../../../autoload.php';
//require __DIR__ . '/../src/services/RoleService.php';

\Tester\Environment::setup();



$roleService = new \Kiwi\Services\RoleService(NULL);

// as null was passed, defaults should be used, so we first get them:
$defaultRoles = \Kiwi\Services\RoleService::getDefaultRoles();

// count of the roles should be the same as the default roles
Assert::same(count($defaultRoles), $roleService->getRoleCount());

// all the default roles should be returned by getAll
foreach ($defaultRoles as $roleIdent => $roleData) {
	Assert::true($roleService->roleExists($roleIdent));
	Assert::same($roleData, $roleService->getByIdent($roleIdent));
}

// check non-existing role
$badRoleIdent = "badRole";
Assert::falsey(array_key_exists($badRoleIdent, $defaultRoles));

Assert::false($roleService->roleExists($badRoleIdent));
Assert::null($roleService->getByIdent($badRoleIdent));
