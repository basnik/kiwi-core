<?php
namespace Kiwi;

use Nette\Security\Permission;

/**
 * Abstract class to define authorizator.
 * To use you have to extend this class and define resources and rules in method build.
 * You also have to register this authorizator in config neon like this:
 *  authorizator: [@App\YourAuthorizator, createAuthorizator] // YourAuthorizator is your class extending KiwiAuthorizator.
 * 
 * This class automatically creates object of Nette\Security\Permission.
 *
 * @author basnik
 */
abstract class KiwiAuthorizator {

	use \Nette\SmartObject;
	
	/** @var array Array of defined roles and their names and descriptions */
	private $roles;
	
	/** @var Permission */
	private $permission;


	/**
	 * Constructor and DI
	 */
	public function __construct() {
		
		$this->roles = array();
		$this->permission = new Permission();

		// here we add reserved admin permission to ensure consistency for backend tools included in Kiwi
		$this->addRole('admin', 'Administrátor', 'Administrátor může všechno.');
		$this->permission->allow('admin', Permission::ALL);

		$this->buildAuthorizator($this->permission);
	}
	
	abstract function buildAuthorizator(Permission $auth);
	
	/**
	 * Returns authorizator.
	 * 
	 * @return Permission
	 */
	public function createAuthorizator(){
		
		return $this->permission;
	}
	
	/**
	 * Adds new role to system.
	 * 
	 * @param string $ident Role ident used in rule definition and source code.
	 * @param string $name Role name displayed to user.
	 * @param string $description Role description. What user can do and with what.
	 * @param array|NULL $parents
	 * @return void
	 */
	protected function addRole($ident, $name, $description, $parents = NULL){
		
		if(array_key_exists($ident, $this->roles)){
			throw new \Nette\InvalidStateException(sprintf('Role with ident %s already exists.', $ident));
		}
		
		$this->permission->addRole($ident, $parents);
		$this->roles[$ident] = array(
			'ident' => $ident,
			'name' => $name,
			'description' => $description
		);
	}
	
	/**
	 * Internal - use RoleService instead.
	 * 
	 * @internal
	 * @return array
	 */
	public function getRoles(){
		
		$this->createAuthorizator();
		
		return $this->roles;
	}
	
}
