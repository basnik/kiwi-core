/**
 * Script for summernote editor image plugin
 * 
 * @author basnik
 */
;(function ($) {

	var insertImage = 'Vložit nebo upravit obrázek';
	
	var thumbOrig = 'originál';
	var thumbBoth = 'napasovat do rozměru';
	var thumbWidth = 'šířka bude';
	var thumbHeight = 'výška bude';
	
	var selectedFile = null;

	$.extend($.summernote.plugins, {
		
		/**
		 * @param {Object} context - context object has status of editor.
		 */
		'kiwiImage': function (context) {

			// ui has renders to build ui elements.
			var ui = $.summernote.ui;
			var $editor = context.layoutInfo.editor;
			
			// modify image popover
			context.options.popover.image = [
			//	['edit', ['kiwiImage']],
				['float', ['floatLeft', 'floatRight', 'floatNone']],
				['remove', ['removeMedia']]
			];
			
			// add image button
			context.memo('button.kiwiImage', function () {
				// create button
				var button = ui.button({
					contents: '<i class="note-icon-picture"/> ',
					tooltip: insertImage,
					click: function () {
						var $dialogBody = $('#jsImageDialogBody').clone().removeClass('hidden');
						var $dialogFooter = $('#jsImageDialogFooter').clone().removeClass('hidden');

						if($dialogBody.length == 0 || $dialogFooter.length == 0){
							throw 'No dialog body and/or footer specified. Unable to open image dialog.';
						}
						
						var text = context.invoke('editor.getSelectedText');
						context.invoke('editor.saveRange');
						
						var dlg = ui.dialog({
							title: insertImage,
							body: $dialogBody.html(),
							footer: $dialogFooter.html()
						});
						
						var $dlg = dlg.render();
						$dlg.appendTo('body');
						$dlg.on('shown.bs.modal', function(){
							initDialogContent($dlg, $editor, context);
							context.triggerEvent('dialog.shown');
						});
						
						ui.showDialog($dlg);
					}
				});

				// create jQuery object from button instance.
				return button.render();
			});
		}
	});
	
	function initDialogContent($dialog, $editor, context){
		

		
		// https://github.com/summernote/summernote/tree/33bf50e17377689d00d46a8dd05b52353447079f/src/js/bs3/module
		
		// todo detekce zda nebyl vybran nejaky prvek - bohuzel to zda se neumi
		if(true){
			selectedFile = null;
			
			$dialog.find('.jsSourceToggle:first').click();
			$dialog.find('.jsImageWidth').val('');
			$dialog.find('.jsImageHeight').val('');
			$dialog.find('.jsExternalUrl').val('');
			$dialog.find('.jsInternalUrl').val('');
			$dialog.find('.jsThumbSizes').val('f');
		}
		
		// todo reset fields / load edited data
	
		if($dialog.hasClass('initComplete')){
			return;
		}
	
		var $internalUrlRow = $dialog.find('.jsInternalUrlRow');
		var $externalUrlRow = $dialog.find('.jsExternalUrlRow');
		var $thumbSizesRow = $dialog.find('.jsThumbSizesRow');
		var $sizesRow = $dialog.find('.jsSizesRow');

		// external / internal url toggle
		$dialog.find('.jsSourceToggle').click(function(){
			if($(this).find('input[type=radio]').val() == 'internal'){
				
				$externalUrlRow.addClass('hidden');
				$sizesRow.addClass('hidden');
				
				$internalUrlRow.removeClass('hidden');
				$thumbSizesRow.removeClass('hidden');
			}else{
				
				$internalUrlRow.addClass('hidden');
				$thumbSizesRow.addClass('hidden');
				
				$externalUrlRow.removeClass('hidden');
				$sizesRow.removeClass('hidden');
			}
		});
		
		// fill thumb sizes
		var $select = $thumbSizesRow.find('select');
		$.get($('#jsThumbSizesLink').val(), function(result){

			$select.children().remove();

			insertOption('f', 'f', thumbOrig);
			$.each(result.b, function(key, item){
				insertOption(item, 'b', thumbBoth + ' ' + item);
			});
			$.each(result.w, function(key, item){
				insertOption(item, 'w', thumbWidth + ' ' + item);
			});
			$.each(result.h, function(key, item){
				insertOption(item, 'h', thumbHeight + ' ' + item);
			});

			function insertOption(value, thumbType, text){
				var $option = $('<option></option>');
				$option.val(value);
				$option.data('thumb', thumbType);
				$option.text(text);
				$select.append($option);
			};
		});
		
		// file select for internal link
		$internalUrlRow.find('.jsFileselectLauncher').click(function(){
			$.kiwi.be.fileSelect({
				imagesOnly: true,
				selectedCallback: function(files){
					selectedFile = files.pop();

					$internalUrlRow.find('.jsInternalUrl').val(getCurrentImageUrl($dialog));
					$internalUrlRow.find('.jsSelectedFileName').removeClass('hidden').text($.kiwi.truncateString(selectedFile.path, 75)).attr('title', selectedFile.path);
					if(selectedFile.isImage){
						var $image = $('<img>');
						var urlPattern = $('#jsImageLink').val();
						
						$image.attr('src', urlPattern.replace('--id--', selectedFile.id).replace('--width--', 100).replace('--height--', 100));
						$internalUrlRow.parent().find('.jsSelectedImageThumb').removeClass('hidden').html('').append($image);
					}
				}
			});
		});
		
		// dialog button actions
		$dialog.find('.jsImageClose').click(function(){
			$dialog.modal('hide');
		});
		$dialog.find('.jsImageInsert').click(function(){
			insertSelectedImage($dialog, $editor, context);
		});
		
		// change url when thumbnail size changes
		$dialog.find('.jsThumbSizes').change(function(){
			var newUrl = getCurrentImageUrl($dialog);
			if(newUrl !== false){
				$dialog.find('.jsInternalUrl').val(newUrl);
			}
		});
		
		$dialog.addClass('initComplete');
	}
	
	function insertSelectedImage($dialog, $editor, context){
		
		var width = 0;
		var height =  0;
		var url = '';
		var alt = $dialog.find('.jsAlt').val();
		var type = 'internal';

		if($dialog.find('.jsSourceToggle input[type=radio]').filter(':checked').val() == 'external'){
			width = $dialog.find('.jsImageWidth').val();
			height = $dialog.find('.jsImageHeight').val();
			url = $dialog.find('.jsExternalUrl').val();
			type = 'external';
			
			if(url.length == 0){
				alert('Vyplňte URL.');
				return;
			}
			if(width.length > 0 && (isNaN(parseInt(width)) || parseInt(width) < 10)){
				alert('Šířka musí být číslo větší než 10.');
				return;
			}
			if(height.length > 0 && (isNaN(parseInt(height)) || parseInt(height) < 10)){
				alert('Výška musí být číslo větší než 10.');
				return;
			}

		}else{
			var $selectedOption = $dialog.find('.jsThumbSizes option:selected');
			var thumbType = $selectedOption.data('thumb');
			var urlPattern = $('#jsImageLink').val();

			if(thumbType == 'b'){
				var dimParts = $selectedOption.val().split('x');
				width = dimParts[0];
				height = dimParts[1];

			}else if(thumbType == 'w'){
				width = $selectedOption.val();

			}else if(thumbType == 'h'){
				height = $selectedOption.val();
			}
			
			if(selectedFile == null){
				alert('Nejprve vyberte nějaký obrázek.');
				return;
			}

			url = urlPattern.replace('--id--', selectedFile.id)
						.replace('--width--', width)
						.replace('--height--', height);
		}

		var attrs = {
			'src': url,
			'alt': alt,
			'data-type': type
		};

		if(type === 'external'){
			if(width.length > 0){
				attrs.width = width;
			}
			if(height.length > 0){
				attrs.height = height;
			}

		}else{
			attrs['data-save-url'] = getCurrentImageUrl($dialog);
		}

		var $imgEl = $('<img />');
		$imgEl.attr(attrs);

		context.invoke('editor.restoreRange');
		context.invoke('editor.insertNode', $imgEl.get(0));
		$dialog.modal('hide');
	}
	
	/**
	 * Constructs image url from options selected in select fields and inserts this url to url field.
	 */
	function getCurrentImageUrl($dialog){
		
		if(selectedFile == null){
			return false;
		}
		
		var $selectedOption = $dialog.find('.jsThumbSizes option:selected');
		var thumbType = $selectedOption.data('thumb');
		var resultLink = 'kiwi://Image ' + selectedFile.id;
		
		if(thumbType == 'b'){
			resultLink += ' ' + $selectedOption.val();

		}else if(thumbType == 'w'){
			resultLink += ' w' + $selectedOption.val();

		}else if(thumbType == 'h'){
			resultLink += ' h' + $selectedOption.val();
		}
		
		return resultLink;
	}

}(jQuery));

