/**
 * Script for kiwi file select tool.
 * 
 * @author basnik
 */
;(function($, window, console){
	

	// namespace check
	if($.kiwi.be.fileSelect){
		throw new 'namespace kiwi.be.fileSelect is already used!';
	}
	
	// define our object
	$.kiwi.be.fileSelect = function(options){
		
		var $element;
		var $content;
		var $contentLoader;
		
		var files = [];
		
		var defaultOptions = {
			'multiple': false,
			'imagesOnly': false,
			'allowedContentTypes': [],
			'selectedCallback': null
		};
		
		function registerContentAjax(){
			$('a.ajax', $content).click(function(event){
				
				event.preventDefault();
				
				$content.addClass('hidden');
				$contentLoader.removeClass('hidden');
				
				var data = {
					imagesOnly: defaultOptions.imagesOnly ? 1 : 0,
					allowedContentTypes: defaultOptions.allowedContentTypes
				};
				
				$.get($(this).attr('href'), data, function(result){
					publishNewContent(result);
				});
			});
			
			// mark clicked form button
			$('form.ajax', $content).find('input[type=submit]').click(function(){
				$('form.ajax', $content).attr('clicked', '0');
				$(this).attr('clicked', '1');
			});
			
			$('form.ajax', $content).submit(function(event){
				
				event.preventDefault();
				
				$content.addClass('hidden');
				$contentLoader.removeClass('hidden');
				
				// get clicked button
				var $target = $('form.ajax', $content).find('input[type=submit][clicked=1]');

				var data = $(this).serialize();
				data += '&' + $target.attr('name') + '=' + $target.val();
				
				$.post($(this).attr('action'), data, function(result){
					publishNewContent(result);
				});
			});
			
			// init all uninited tooltips
			$.kiwi.be.initTooltips();
		}
		
		function publishNewContent(content){
			
			$contentLoader.addClass('hidden');
			$content.html(content.template).removeClass('hidden');
			
			files = content.files;
			
			registerContentAjax();
			
			// mark checkbox by its row click, beware of multiple option
			$content.find('tr').click(function(event){
				var $checkbox = $(this).find('input[type=checkbox]');
				var checkboxClicked = $(event.target).is('input[type=checkbox]');
				if($checkbox.length > 0){
					if($checkbox.is(':checked')){
						if(!checkboxClicked){
							$checkbox.prop('checked', false); 
						}else if(defaultOptions.multiple === false){
							$content.find('input[type=checkbox]').prop('checked', false);
							$checkbox.prop('checked', true);
						}
					}else{
						if(defaultOptions.multiple === false){
							$content.find('input[type=checkbox]').prop('checked', false);
						}
						if(!checkboxClicked){
							$checkbox.prop('checked', true);
						}
					}
				}
			});
		}
		
		var self = {
			init: function(options){
				
				$.extend( defaultOptions, options );
				
				$element = $('#jsFileSelectModal').clone();
				$('body').append($element);
				$element.modal();
				
				$content = $element.find('.jsModalContent');
				$contentLoader = $element.find('.jsModalContentLoader');
				
				var screenHeight = $(window).height();
				// obsah bude vysoky 70% obrazovky
				$content.css({
					'overflow-y' : 'auto',
					'max-height': (screenHeight*0.7)+'px'
				});
				
				$element.on('hidden.bs.modal', function(){
					$element.remove();
				});
				
				var data = {
					imagesOnly: defaultOptions.imagesOnly ? 1 :0,
					allowedContentTypes: defaultOptions.allowedContentTypes
				};
				
				// load content
				$.get($('#jsFileSelectLink').val(), data, function(result){
					publishNewContent(result);
				});
				
				// proccess selected files
				$element.find('.jsFsSelectFiles').click(function(){
					if(console && (defaultOptions.selectedCallback === null || typeof defaultOptions.selectedCallback !== 'function')){
						console.log('No callback set or it is not a function');
					}
					
					var resultFiles = [];
					$element.find('input[type=checkbox]:checked').each(function(key, item){
						resultFiles.push(files[$(item).val()]);
					});
					
					if(resultFiles.length === 0){
						alert('Vyberte soubor.');
						return;
					}
					
					defaultOptions.selectedCallback.call(null, resultFiles);
					$element.modal('hide');
				});
			}
		};
		
		self.init(options);
		
		return self; 
	};
	
}(jQuery, window, console));
