/**
 * Main javascript file of Kiwi.
 * Defines kiwi namespace, provides basic methods
 * 
 * @author basnik
 */
;(function ( $, document, undefined){
	
	// namespace check
	if($.kiwi){
		throw new 'kiwi namespace of jquery is already used!';
	}
	if($.fn.kiwi){
		throw new 'kiwi namespace for jquery plugins is already used!';
	}
	
	// define our namespaces
	$.kiwi = {};
	$.fn.kiwi = {};
	
	
	// define public methods
	$.kiwi.formatByteSize = function(bytes){
		if (typeof bytes !== 'number') {
			return '';
		}
		if (bytes >= 1000000000) {
			return (bytes / 1000000000).toFixed(2) + ' GB';
		}
		if (bytes >= 1000000) {
			return (bytes / 1000000).toFixed(2) + ' MB';
		}
		return (bytes / 1000).toFixed(2) + ' kB';
	};
	
	$.kiwi.truncateString = function(string, length, endString){
		if(endString === undefined){
			endString = ' ...';
		}
		return string.substr(0, length) + (string.substr(length).length > 0 ? endString : '');
	};
	
	// call default actions
	$(document).ready(function(){
		
		// init ajax, etc.
		$.nette.init();
		
		// and we are loaded ...
		$(document).trigger('kiwi.core.loaded');
		
		// auto confirms
		$(document).on('click', 'a[data-confirm]', function(){
			return confirm($(this).data('confirm')) === true ? true : false;
		});
	});
	
})(jQuery, document);
