<?php
namespace Kiwi\Be;

/**
 * Used for DI.
 * 
 * @internal
 * @author basnik
 */
interface IFileselectGridFactory {
	
	/** @return \Kiwi\Be\FileselectGrid */
	public function create();
}
