<?php

namespace Kiwi\Entities;

use Basnik\Db\Entity;

/**
 * User storage.
 */
class User extends Entity {

	/** 
	 * List of user roles - array of their idents
	 */
	public $roles = [];

	/**
	 * Email is used also as username
	 */
	public $email;

	/**
	 * User name which is displayed, when it is filled. Not mandatory.
	 */
	public $name;

	/**
	 * Encrypted user password is stored here, if it is null, user cannot login (when user is created, but no password was sent to him yet)
	 */
	public $password;

	/**
	 * If user is blocked or not. Blocked users cannot log in.
	 */
	public $blocked = FALSE;

	/**
	 * If password change is required.
	 */
	public $pwdChangeRequired = NULL;

	/**
	 * Stores json encoded additional user data - added by plugins - array
	 */
	public $data = [];


	/**
	 * Converts database row obtained as array to object.
	 * @param array $input
	 * @return User
	 */
	public static function load(array $input) {
		$user = new User();
		$user->extractKeys($input, "id", "email", "name", "password", "blocked", "pwdChangeRequired");
		$user->roles = [];
		$user->data = [];
		if (array_key_exists("roles", $input)) {
			$user->roles = json_decode($input["roles"], TRUE) ?: [];
		}
		if (array_key_exists("data", $input)) {
			$user->data = json_decode($input["data"], TRUE) ?: [];
		}
		return $user;
	}

	/**
	 * Returns associative array with properties of this object - array should be used to store the model into database.
	 * @return array
	 */
	public function save() : array {
		$result = $this->exportProps( "id", "email", "name", "password", "blocked", "pwdChangeRequired");
		$result["roles"] = json_encode($this->roles);
		$result["data"] = json_encode($this->data);
		return $result;
	}
}
