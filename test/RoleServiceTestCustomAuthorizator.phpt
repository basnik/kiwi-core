<?php
namespace Kiwi\Test;

use \Tester\Assert;

require __DIR__ . '/../../../autoload.php';
//require __DIR__ . '/../src/services/RoleService.php';

\Tester\Environment::setup();

// our testing roles
$testingRoles = [
	'editor' => [
		'ident' => 'editor',
		'name' => 'Editor',
		'description' => 'Some editor note.'
	],
	'reduced' => [
		'ident' => 'reduced',
		'name' => 'Reduced access',
		'description' => 'Some reduced user note.'
	]
];

// test custom authorizator
class TestAuthorizator extends \Kiwi\KiwiAuthorizator {

	public function buildAuthorizator(\Nette\Security\Permission $auth) {

		// here we use this little hack, as this is only for testing purposes
		global $testingRoles;

		foreach ($testingRoles as $roleIdent => $roleData) {
			$this->addRole($roleIdent, $roleData['name'], $roleData['description']);
		}
	}
}


$testAuthorizator = new TestAuthorizator();
$roleService = new \Kiwi\Services\RoleService($testAuthorizator);

// there should be all testing roles + one more role for admin
Assert::same(count($testingRoles) + 1, $roleService->getRoleCount());

// there should be all the roles from testing authorizator
foreach ($testingRoles as $roleIdent => $roleData) {
	Assert::true($roleService->roleExists($roleIdent));
	Assert::same($roleData, $roleService->getByIdent($roleIdent));
}

// plus admin role should be added automatically
Assert::true($roleService->roleExists('admin'));

// check non-existing role
$badRoleIdent = "badRole";
Assert::falsey(array_key_exists($badRoleIdent, $testingRoles));

Assert::false($roleService->roleExists($badRoleIdent));
Assert::null($roleService->getByIdent($badRoleIdent));
