<?php

namespace Kiwi\Services;

use Basnik\Db\Service;
use Kiwi\Entities\User;

/**
 * Description of Users
 *
 * @author basnik
 */
class UserService extends Service {

	public function __construct(\Dibi\Connection $db) {
		parent::__construct($db, "kw_users", User::class);
	}
	
	/**
	 * Gets one user by given e-mail.
	 * @param string $email
	 * @return User|NULL
	 */
	public function getByEmail($email) {
		return $this->fetchObject(
			"SELECT * FROM %n WHERE email = %s",
			$this->mainTable,
			$email
		);
	}
	
	/**
	 * Gets one user by id sha1 hash.
	 * @param string $hash
	 * @return User|NULL
	 */
	public function getByIdHash($hash) {
		return $this->fetchObject(
			"SELECT * FROM %n WHERE SHA1(id) = %s",
			$this->mainTable,
			$hash
		);
	}

	/**
	 * Returns given password hashed. Uses native PHP 5 implementation
	 * http://php.net/manual/en/book.password.php
	 * For PHP between 5.3.7 - 5.5 uses compatability library:
	 * https://github.com/ircmaxell/password_compat
	 * Used algorithm could (maybe in future) produce hashes up to 255 characters long!
	 * 
	 * @param string $password Password to hash
	 * @return string Hashed password.
	 */
	public function hashPassword($password){
		$hash = password_hash($password, PASSWORD_DEFAULT);
		if($hash === FALSE){
			throw new \Kiwi\PasswordNotGeneratedException();
		}
		return $hash;
	}
	
	/**
	 * Generates random 10 characters long password. Can be used as starting password for users.
	 * @todo toto se zmenou na nezasilani hesel mailem nebude treba
	 * @return string
	 */
	public function generateRandomPassword(){
		return \Nette\Utils\Strings::random(10, '0-9a-z_?!+%');
	}
}
