<?php

namespace Kiwi;

use Kiwi\Services\SystemService;
use Nette\Security\IAuthenticator;

/**
 * Backend users authenticator.
 */
class Authenticator {

    use \Nette\SmartObject;

	/** @var \Kiwi\Services\UserService */
	protected $userService;
	
	/** @var Services\RoleService */
	protected $roleService;
	
	/** @var \Nette\Security\User */
	protected $user;

	/** @var SystemService */
	protected $system;

	public function __construct(\Nette\Security\User $user, \Kiwi\Services\UserService $userService, \Kiwi\Services\RoleService $roleService, SystemService $system){
		
		$this->user = $user;
		$this->userService = $userService;
		$this->roleService = $roleService;
		$this->system = $system;
	}

	/**
	 * Performs an authentication.
	 * 
	 * @param string $email Used as username
	 * @param string $password User password
	 * 
	 * @return void
	 * @throws Nette\Security\AuthenticationException
	 */
	public function login($email, $password)
	{

		$user = $this->userService->getByEmail($email);

		if ($user === NULL) {
			throw new \Nette\Security\AuthenticationException('Takový e-mail nepatří žádnému uživateli.', IAuthenticator::IDENTITY_NOT_FOUND);
		}

		if (!password_verify($password, $user->password)) {
			throw new \Nette\Security\AuthenticationException('Nesprávné heslo.', IAuthenticator::INVALID_CREDENTIAL);
		}
		
		$userData = array(
			'id' => $user->id,
			'name' => $user->name,
			'email' => $user->email,
			'data' => $user->data
		);


		$activeRoles = array();
		foreach($user->roles as $role){
			if($this->roleService->roleExists($role)){
				$activeRoles[] = $role;
			}
		}

		$this->user->login(new Identity($user->id, $this, $activeRoles, $userData));

		$this->system->logActivity(sprintf("Uživatel %s (%s, %d) přihlášen.", $user->email, $user->name, $user->id));
	}

}
