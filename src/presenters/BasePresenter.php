<?php
namespace Kiwi;

use IPub\AssetsLoader\LoaderFactory;
use Kiwi\Entities\User;
use Nette\Utils\DateTime;
use Nette\Mail\Message;
use Nette\Mail\SendmailMailer;

/**
 * Base presenter is parent for every presenter used in Kiwi-based applications.
 *
 * @author basnik
 */
abstract class BasePresenter extends \Nette\Application\UI\Presenter {
	
	/** @var \Kiwi\Services\SystemService */
	protected $system;
	
	/**
	 * @param \Kiwi\Services\SystemService $service
	 */
	public function injectSystemService(\Kiwi\Services\SystemService $service){
		$this->system = $service;
	}
	
	public function beforeRender(){
		parent::beforeRender();
		
		$this->template->now = new \DateTime();
		$this->template->kiwiVersion = Services\SystemService::VERSION;
		$this->template->kiwiResourcesMode = $this->system->getResourcesMode();

		if($this->user->isLoggedIn()){
			$this->template->loggedUserData = $this->user->getIdentity()->getData();
		}
		
		if($this->isAjax()){
			$this->redrawControl('flashes');
		}
	}
	
	/**
	 * Sends e-mail with link to reset user password. Returns datetime till when the link is valid.
	 * 
	 * @param \Kiwi\User $to
	 * @return \Kiwi\DateTime
	 */
	protected function sendPassMail(User $to){

		$webName = $this->system->getWebName();
		$fromMail = $this->system->getDefaultFromMail();
		
		$requestValidTill = new DateTime();
		$requestValidTill->add(new \DateInterval('P2W'));
		
		$mail = new Message();
		$mail->setFrom($webName.' <'.$fromMail.'>');
		$mail->addTo($to->email);
		
		$mailTpl = $this->createTemplate();
		$mailTpl->setFile(__DIR__.'/../templates/mailPass.latte');
		
		$mailTpl->userHash = sha1($to->id);
		$mailTpl->webName = $this->system->getWebName();
		$mailTpl->validTill = $requestValidTill;
		
		$mail->setHTMLBody((string) $mailTpl);
		
		$mailer = new SendmailMailer();
		$mailer->send($mail);
		
		return $requestValidTill;
	}

}
