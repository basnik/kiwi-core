<?php

namespace Kiwi\Services;

use Basnik\Db\BaseEntity;
use Basnik\Db\BaseService;
use Kiwi\Entities\File;

/**
 * Description of Kiwi
 *
 * @author basnik
 */
class InternalFileService extends InternalFilesystemService {
	
	public function __construct(\Dibi\Connection $db) {
		parent::__construct($db, "kw_files", File::class);
	}
}