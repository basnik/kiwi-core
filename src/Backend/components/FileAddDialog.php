<?php
namespace Kiwi\Be;

use Nette\Application\UI\Form;

use Kiwi\Entities\Directory;
use Kiwi\Services\FilesystemService;
use Kiwi\Services\SystemService;

/**
 * Description of FileAddDialog
 *
 * @author basnik
 */
class FileAddDialog extends \Nette\Application\UI\Control {
	
	/** @var array On change save callback*/
	public $onSaveSuccess;
	
	/** @var array On failed file callback*/
	public $onFileError;
	
	/** @var Directory */
	protected $parentDir;
	
	/** @var FilesystemService */
	protected $filesystem;
	
	/** @var \Kiwi\Services\SystemService */
	protected $system;
	
	/** @var boolean */
	protected $open = FALSE;
	
	/** @var boolean */
	protected $showSubmit = TRUE;

	/** @var \Nette\Security\User */
	protected $user;

	
	/**
	 * Constructor used by DI.
	 * 
	 * @param FilesystemService $filesystem
	 * @param \Nette\ComponentModel\IContainer $parent
	 * @param type $name
	 */
	public function __construct(FilesystemService $filesystem, SystemService $system,
								\Nette\Security\User $user, \Nette\ComponentModel\IContainer $parent = NULL, $name = NULL) {
		$this->filesystem = $filesystem;
		$this->system = $system;
		$this->user = $user;
	}
	
	/**
	 * Called right after dialog creation.
	 * 
	 * @param Directory $parentDir
	 */
	public function setDirectory(Directory $parentDir=NULL){
		$this->parentDir = $parentDir;
	}
	
	/**
	 * Opens the dialog.
	 */
	public function open(){
		$this->open = TRUE;
	}
	
	/**
	 * Closes the dialog.
	 */
	public function close(){
		$this->open = FALSE;
	}
	
	
	public function render(){
		
		$this->template->_form = $this->template->form = $this['addFilesForm'];
		$this->template->open = $this->open;
		$this->template->showSubmit = $this->showSubmit;
		$this->template->setFile(__DIR__.'/../templates/components/fileAddDialog.latte');
		$this->template->render();
	}
	

	public function createComponentAddFilesForm(){
		
		$dirForm = new Form();
		$dirForm->getElementPrototype()->class = 'ajax';
		
		$dirForm->addMultipleUpload('uploadedFiles', NULL, $this->system->getTempDirectory().'/mfu-files');
		$dirForm->addSubmit('addUploadedFiles');
		
		$dirForm->onSuccess[] = [$this, "addFilesFormSubmitted"];

		return $dirForm;
	}
	
	public function addFilesFormSubmitted($form){

		$files = $form->values->uploadedFiles;
		$allFilesOk = FALSE;
		if(!empty($files)){
			
			$allFilesOk = TRUE;
			foreach($files as $file){
				try{
					$newFile = $this->filesystem->addFile($this->parentDir->id ?? NULL, $file->getPath(),
						$this->user->id, $file->getName(), $file->getSize());

					$this->system->logActivity(sprintf("Nahrán soubor %s (%d) do složky %s.",
						$newFile->name, $newFile->id, $this->presenter->getDirName($this->parentDir->id ?? NULL)));

				}catch(\Kiwi\DuplicateException $e){
					$allFilesOk = FALSE;
					$this->flashMessage('Soubor '.$e->getDuplicateName().' již v této složce existuje a byl přeskočen, ostatní soubory byly nahrány.', 'danger');
					$this->showSubmit = FALSE;
					$this->redrawControl('flashes');
					$this->redrawControl('buttons');
					
					$this->onFileError();
				}
			}
		}else{
			$this->flashMessage('Žádné soubory k nahrání.', 'danger');
			$this->redrawControl('flashes');
		}

		if($allFilesOk){
			$this->onSaveSuccess();
		}
	}
	
	
}
