<?php
namespace Kiwi;

/**
 * Base class for all filesystem single pickers, handles simmilar functionality of children classes.
 *
 * @author basnik
 */
class BaseFilesystemSinglePicker extends BaseFilesystemPicker {
	
	
	/**
	 * Gets control from parent and perform common picker actions on it
	 * @return \Nette\Utils\Html
	 */
	public function getControl(): \Nette\Utils\Html {
		$control = parent::getControl();
		$control->value = (string) $this->value;
		return $control;
	}
	
	/**
	 * Sets control's value.
	 * @param  string
	 * @return self
	 * @internal
	 */
	public function setValue($value)
	{
		if (empty($value)) {
			$value = 0;
		} elseif (!is_numeric($value)) {
			throw new \Nette\InvalidArgumentException(sprintf("Value must be numeric or NULL, %s given in field '%s'.", gettype($value), $this->name));
		}
		$this->value = $value;
		return $this;
	}


	/**
	 * Returns control's value.
	 * @return string
	 */
	public function getValue()
	{
		return intval($this->value);
	}
}
