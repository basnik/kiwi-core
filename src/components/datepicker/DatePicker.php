<?php
namespace Kiwi;

use Nette\Utils\Html;
use Nette\Forms\Controls\TextInput;

/**
 * Adds date input with bootstrap datepicker
 * @see https://github.com/eternicode/bootstrap-datepicker
 * 
 * @author basnik
 */
class DatePicker extends TextInput{

	/** @var string datepicker date format */
	protected $format;

	/** @var string render from this date */
	protected $renderFrom;
	
	/** @var string render till this date */
	protected $renderTill;

	/** @var array Array of disabled day numbers - beggining from zero */
	protected $disableDays;
	
	/** @var boolean whether to show week numbers in calendar*/
	protected $showWeekNumbers;
	
	/**
	 * Gets control.
	 * 
	 * @return DatePicker
	 */
	public function getControl(): \Nette\Utils\Html {
		
		$parent = Html::el('div', array( 'class' => 'input-group date jsKiwiDpCont'));
		
		$control = parent::getControl();
		$control->class('jsKiwiDatePicker', TRUE);
		
		// set protected properties as data attributes to parse in javascript
		$attrs = array();
		if(isset($this->format)){
			$attrs['data-date-format'] = $this->format;
		}
		if(isset($this->renderFrom)){
			$attrs['data-date-start-date'] = $this->renderFrom;
		}
		if(isset($this->renderTill)){
			$attrs['data-date-end-date'] = $this->renderTill;
		}
		if(isset($this->disableDays)){
			$attrs['data-date-days-of-week-disabled'] = implode(',', $this->disableDays);
		}
		if(isset($this->showWeekNumbers) && $this->showWeekNumbers === TRUE){
			$attrs['data-date-calendar-weeks'] = 1;
		}
		$control->addAttributes($attrs);
		
		$button = Html::el('span', array( 'class' => 'input-group-addon jsKiwiDpTrigger'));
		$icon = Html::el('i', array( 'class' => 'ico ico-calendar'));
		$button->addHtml($icon);

		$parent->addHtml($control);
		$parent->addHtml($button);
		
		return $parent;
	}
	
	/**
	 * Sets datepicker date format
	 * 
	 * @see http://bootstrap-datepicker.readthedocs.org/en/latest/options.html#format
	 * @param string $format
	 * @return \Kiwi\DatePicker\DatePicker
	 */
	public function setFormat($format){
		$this->format = $format;
		return $this; // fluent
	}

	/**
	 * Sets beginning date for the calendar. If this is not set, infinite calndar is rendered in the past.
	 * Must be in format set by setFormat.
	 * 
	 * @see http://bootstrap-datepicker.readthedocs.org/en/latest/options.html#id6
	 * @param string $from
	 * @return \Kiwi\DatePicker\DatePicker
	 */
	public function setRenderFrom($from){
		$this->renderFrom = $from;
		return $this; // fluent
	}
	
	/**
	 * Sets ending date for the calendar. If this is not set, infinite calndar is rendered in the future.
	 * Must be in format set by setFormat.
	 * 
	 * @see http://bootstrap-datepicker.readthedocs.org/en/latest/options.html#id5
	 * @param string $till
	 * @return \Kiwi\DatePicker\DatePicker
	 */
	public function setRenderTill($till){
		$this->renderTill = $till;
		return $this; // fluent
	}
	
	/**
	 * Sets disabled days in week, starting from zero.
	 * e.g. setDisabledDays(array('0','6')) disables saturday and sunday.
	 * 
	 * @see http://bootstrap-datepicker.readthedocs.org/en/latest/options.html#defaultviewdate
	 * @param array $days
	 * @return \Kiwi\DatePicker\DatePicker
	 */
	public function setDisabledDays(array $days){
		$this->disableDays = $days;
		return $this; // fluent
	}
	
	/**
	 * Sets if week numbers are shown in the calendar.
	 * Week numbers are disabled by default.
	 * 
	 * @see http://bootstrap-datepicker.readthedocs.org/en/latest/options.html#calendarweeks
	 * @param boolean $show
	 * @return \Kiwi\DatePicker\DatePicker
	 */
	public function showWeekNumbers($show=TRUE){
		$this->showWeekNumbers = $show;
		return $this; // fluent
	}

}
