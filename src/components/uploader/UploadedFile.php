<?php

namespace Kiwi;


/**
 * Container object for file uploads
 *
 * @author basnik
 */
class UploadedFile {

    use \Nette\SmartObject;
	
	/** @var string Key - name in tmp directory after upload */
	protected $key;
	
	/** @var string Original filename */
	protected $name;

	/** @var string Path to file in upload directory */
	protected $path;
	
	/** @var int File size in bytes */
	protected $size;
	
	public function __construct($key, $name, $path, $size) {
		$this->key = $key;
		$this->name = $name;
		$this->path = $path;
		$this->size = $size;
	}
	
	/**
	 * Returns StdClass to be converted to json
	 * @return \stdClass
	 */
	function toJSON(){
		$output = new \stdClass();
		$output->key = $this->key;
		$output->name = $this->name;
		$output->size = $this->size;
		$output->path = $this->path;
		return $output;
	}
	
	function getKey() {
		return $this->key;
	}

	function getName() {
		return $this->name;
	}

	function getPath() {
		return $this->path;
	}

	function getSize() {
		return $this->size;
	}
}
