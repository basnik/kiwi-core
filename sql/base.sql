-- plain database to start using kiwi


SET NAMES utf8;
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

CREATE TABLE `kw_directories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `owner_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_76AE23C1727ACA70` (`parent_id`),
  KEY `owner_id` (`owner_id`),
  CONSTRAINT `FK_76AE23C1727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `kw_directories` (`id`),
  CONSTRAINT `kw_directories_ibfk_1` FOREIGN KEY (`owner_id`) REFERENCES `kw_users` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `kw_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `directory_id` int(11) DEFAULT NULL,
  `owner_id` int(11) DEFAULT NULL,
  `name` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `storage_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_image` tinyint(1) NOT NULL,
  `size` int(11) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_FCA380472C94069F` (`directory_id`),
  KEY `owner_id` (`owner_id`),
  CONSTRAINT `FK_FCA380472C94069F` FOREIGN KEY (`directory_id`) REFERENCES `kw_directories` (`id`),
  CONSTRAINT `kw_files_ibfk_1` FOREIGN KEY (`owner_id`) REFERENCES `kw_users` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `kw_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roles` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:simple_array)',
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `blocked` tinyint(1) NOT NULL,
  `pwd_change_required` datetime DEFAULT NULL,
  `data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json_array)',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_EE1565F7E7927C74` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `kw_values` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ident` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:json_array)',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_C052E74544E78B2` (`ident`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- default user with default password - kiwi
INSERT INTO `kw_users` (`id`, `email`, `name`, `password`, `blocked`, `pwd_change_required`, `data`, `roles`) VALUES
(1,	'default@kiwi.cz',	'Admin',	'$2y$10$V4PNs5MOSFG532FMkIoHLu8bzQ7622stVf/1ddK7aQrFb4OWrgeKW',	0,	NULL,	'[]',	'[\"admin\"]');

