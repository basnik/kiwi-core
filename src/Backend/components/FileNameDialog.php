<?php
namespace Kiwi\Be;

use Kiwi\Services\SystemService;
use Nette\Application\ForbiddenRequestException;
use Tomaj\Form\Renderer\BootstrapVerticalRenderer;
use Nette\Application\UI\Form;

use Kiwi\Entities\Directory;
use Kiwi\Services\FilesystemService;

/**
 * Description of FileAddDialog
 *
 * @author basnik
 */
class FileNameDialog extends \Nette\Application\UI\Control {
	
	/** @var array On change save callback*/
	public $onSaveSuccess;
	
	/** @var Directory */
	protected $parentDir;
	
	/** @var FilesystemService */
	protected $filesystem;
	
	/** @var string Dialog title */
	protected $title = '';
	
	/** @var string Dialog input label */
	protected $inputLabel = '';
	
	/** @var \Kiwi\Entities\File | \Kiwi\Entities\Directory */
	protected $editedEntity = NULL;
	
	/** @var boolean */
	protected $open = FALSE;

	/** @var \Nette\Security\User */
	protected $user;

	/** @var \Kiwi\Services\SystemService */
	protected $system;
	
	/**
	 * Constructor used by DI.
	 * 
	 * @param FilesystemService $filesystem
	 * @param \Nette\ComponentModel\IContainer $parent
	 * @param type $name
	 */
	public function __construct(FilesystemService $filesystem, \Nette\Security\User $user, SystemService $system, \Nette\ComponentModel\IContainer $parent = NULL, $name = NULL) {
		$this->filesystem = $filesystem;
		$this->user = $user;
		$this->system = $system;
	}
	
	/**
	 * Called right after dialog creation.
	 * 
	 * @param Directory $parentDir
	 */
	public function setDirectory(Directory $parentDir=NULL){
		$this->parentDir = $parentDir;
	}
	
	/** 
	 * Sets dialog title
	 * 
	 * @param string $title
	 * @return void
	 */
	public function setTitle($title){
		$this->title = $title;
	}
	
	/**
	 * Opens the dialog.
	 */
	public function open(){
		$this->open = TRUE;
	}
	
	/**
	 * Closes the dialog.
	 */
	public function close(){
		$this->open = FALSE;
	}
	
	/** 
	 * Set edited directory
	 * 
	 * @param int $id Sets edited directory id
	 * @return void
	 */
	public function setEditedDirectory($id){
		$directory = $this->filesystem->getDirectoryById($id);
		if($directory === NULL){
			throw new \Nette\InvalidArgumentException('Invalid directory id ('.$id.').');
		}
		
		$this->editedEntity = $directory;
	}
	
	/** 
	 * Set edited file
	 * 
	 * @param int Sets edited directory id
	 * @return void
	 */
	public function setEditedFile($id){
		$file = $this->filesystem->getFileById($id);
		if($file === NULL){
			throw new \Nette\InvalidArgumentException('Invalid file id ('.$id.').');
		}
		
		$this->editedEntity = $file;
	}
	
	/** 
	 * Sets input label
	 * 
	 * @param string $label
	 * @return void
	 */
	public function setInputLabel($label){
		$this->inputLabel = $label;
	}
	
	public function render(){
		
		$this->template->dialogTitle = $this->title;
		$this->template->inputLabel = $this->inputLabel;
		$this->template->open = $this->open;
		
		$this->template->setFile(__DIR__.'/../templates/components/fileNameDialog.latte');
		$this->template->render();
	}
	
	public function createComponentNameInputForm(){
		
		$dirForm = new Form();
		$dirForm->setRenderer(new BootstrapVerticalRenderer());
		
		$dirForm->getElementPrototype()->class = 'ajax';

		$dirForm->addText('name')
			->addRule(Form::MAX_LENGTH, 'Maximálně %d znaků.', 50)
			->setRequired('Zadejte prosím jméno.');
		
		$dirForm->addHidden('editId', $this->editedEntity ? $this->editedEntity->id : 0);
		$dirForm->addHidden('editType', $this->editedEntity instanceof \Kiwi\Entities\File ? 'f' : 'd');
		
		$dirForm->addSubmit('addDir');
		
		if($this->editedEntity !== NULL){
			$dirForm->setDefaults(array(
				'name' => $this->editedEntity->name
			));
		}
		
		$dirForm->onSuccess[] = [$this, "nameInputFormSubmitted"];

		return $dirForm;
	}
	
	public function nameInputFormSubmitted(Form $form){

		$values = $form->getValues();
		$isFile = $values->editType !== 'd';
		
		try{
			
			$message = '';
			if(!$isFile){
				
				if(empty($values->editId)){
					$this->editedEntity = new Directory();
					$this->editedEntity->parentId = $this->parentDir->id ?? NULL;
					$this->editedEntity->ownerId = $this->user->id;
					$this->editedEntity->name = $values['name'];
					$this->editedEntity->created = new \DateTime();
					$this->filesystem->saveDirectory($this->editedEntity);

					$this->system->logActivity(sprintf('Nová složka %s(%d) byla vytvořena ve složce %s.',
						$this->editedEntity->name, $this->editedEntity->id, $this->presenter->getDirName($this->parentDir->id ?? NULL)));
					
					$message = sprintf('Nová složka %s byla vytvořena.', $this->editedEntity->name);
				}else{
					
					$this->setEditedDirectory($values->editId);

					if ($this->editedEntity->ownerId != $this->user->id && !$this->user->isInRole('admin')) {
						throw new ForbiddenRequestException("User not allowed to rename directory ".$values->editId);
					}

					$oldName = sprintf("%s (%d)", $this->editedEntity->name, $this->editedEntity->id);
					$this->editedEntity->name = $values['name'];
					$this->filesystem->saveDirectory($this->editedEntity);

					$this->system->logActivity(sprintf('Složka %s byla přejmenována na %s ve složce %s.',
						$oldName, $this->editedEntity->name, $this->presenter->getDirName($this->parentDir->id ?? NULL)));
					
					$message = sprintf('Složka byla přejmenována na %s.', $this->editedEntity->name);
				}
			}else{
				
				$this->setEditedFile($values->editId);

				if ($this->editedEntity->ownerId != $this->user->id && !$this->user->isInRole('admin')) {
					throw new ForbiddenRequestException("User not allowed to rename file ".$values->editId);
				}

				$oldName = sprintf("%s (%d)", $this->editedEntity->name, $this->editedEntity->id);
				$this->editedEntity->name = $values['name'];
				$this->filesystem->saveFile($this->editedEntity);

				$this->system->logActivity(sprintf('Soubor %s byl přejmenován na %s ve složce %s.',
					$oldName, $this->editedEntity->name, $this->presenter->getDirName($this->parentDir->id ?? NULL)));
				
				$message = sprintf('Soubor byl přejmenován na %s.', $this->editedEntity->name);
			}
			
			$this->close();
			$this->onSaveSuccess($message);
			
		}catch(\Kiwi\DuplicateException $e){
			$this->flashMessage((!$isFile ? 'Složka' : 'Soubor').' '.$e->getDuplicateName().' již v této složce existuje. Zvolte jiné jméno.', 'danger');
			$this->redrawControl('flashes');
		}
	}
}
