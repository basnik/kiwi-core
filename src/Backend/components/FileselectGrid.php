<?php

namespace Kiwi\Be;

class FileselectGrid extends AFilesGrid{

	
	/** @var int Filter for fileSelect */
	protected $imagesOnly;
	
	/** @var array Filter for fileSelect */
	protected $allowedContentTypes;
	
	
	public function __construct(\Kiwi\Services\FilesystemService $filesystem, \Nette\Http\Session $session) {
		parent::__construct($filesystem, $session);
		
		// set up the grid
		$this->setRowPrimaryKey('id');
		$this->addCellsTemplate(__DIR__.'/../templates/Files/fileSelectGrid.latte');

		$this->addColumn('id', ' ');
		$this->addColumn('name', 'Jméno')->enableSort();
		$this->addColumn('size', 'Velikost')->enableSort();
		$this->addColumn('ownerName', 'Vlastník')->enableSort();
		$this->addColumn('created', 'Vytvořeno')->enableSort();
		
		$this->setFilterFormFactory(function() {
			
			$form = new \Nette\Forms\Container();

			$form->addText('name');
			$form->addText('size');
			$form->addSelect('comparator', NULL, array(
				'gt' => '>',
				'lt' => '<'
			));
			$form->addSelect('units', NULL, array(
				'k' => 'kB',
				'B' => 'B',
				'M' => 'MB'
			));

			$form->addText('ownerName');

			$form->addSubmit('filter', 'Filtrovat');
			$form->addSubmit('cancel', 'Zrušit filtr');

			return $form;
		});
	}
	
	public function setParameters($imagesOnly=0, array $allowedTypes=array()){
		$this->imagesOnly = $imagesOnly;
		$this->allowedContentTypes = $allowedTypes;
	}
	
	protected function getRawDataCollection() {
		return $this->filesystem->getDirectoryContent($this->parentDir->id ?? NULL, $this->imagesOnly, $this->allowedContentTypes);
	}

}
