<?php

namespace Kiwi;

class PasswordNotGeneratedException extends \RuntimeException {}

class UnknownModuleException extends \RuntimeException {}

class ConfigurationException extends \ErrorException {}

class ThumbsizeNotAllowedException extends \Nette\InvalidArgumentException {}

class UploadException extends \Nette\InvalidArgumentException {}

class DuplicateException extends \Nette\InvalidArgumentException {
	
	protected $duplicateName;
	
	public function __construct($message, $code, $previous, $duplicateName) {
		parent::__construct($message, $code, $previous);
		$this->duplicateName = $duplicateName;
	}
	
	public function getDuplicateName(){
		return $this->duplicateName;
	}
}