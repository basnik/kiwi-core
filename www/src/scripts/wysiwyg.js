/**
 * Custom script to get text editor for kiwi to work.
 * Uses summernote
 * @see http://summernote.org/#/
 * 
 * @author basnik
 */
;(function($, document){
	$(document).ready(function(){
		
		var TOOLBAR_BASIC = 1;
		var TOOLBAR_ADVANCED = 5;

		$('.jsKiwiTextEditor:not(.hasEditor)').each(function(key, item){
			
			var width = $(item).data('wysiwygWidth');
			var height = $(item).data('wysiwygHeight');
			var minHeight = $(item).data('wysiwygMinHeight');
			var maxHeight = $(item).data('wysiwygMaxHeight');
			var disableResize = $(item).data('wysiwygNoResize');
			var toolbar = $(item).data('wysiwygToolbar');
			var fullScreen = $(item).data('wysiwygEnableFull');
			var codeView = $(item).data('wysiwygEnableCodeview');
			
			var options = {
				lang: 'cs-CZ'
			};
			var toolbarButtons = [];
			
			if(typeof width !== 'undefined'){
				options.width = width;
			}
			if(typeof height !== 'undefined'){
				options.height = height;
			}
			if(typeof minHeight !== 'undefined'){
				options.minHeight = minHeight;
			}
			if(typeof maxHeight !== 'undefined'){
				options.maxHeight = maxHeight;
			}
			if(typeof disableResize !== 'undefined' && disableResize == 1){
				options.disableResizeEditor = true;
			}
			if(typeof toolbar !== 'undefined'){
				switch(toolbar){
					case TOOLBAR_BASIC:
						toolbarButtons = [
							['style', ['bold', 'italic', 'underline', 'clear']],
							['fontsize', ['fontsize']],
							['insert', ['kiwiLink']]
						];
						break;
					case TOOLBAR_ADVANCED:
						toolbarButtons = [
							['style', ['bold', 'italic', 'underline', 'clear'], ['superscript','subscript']],
							['subsup', ['superscript','subscript']],
							['fontsize', ['fontsize']],
							['para', ['ul', 'ol', 'paragraph']],
							['height', ['height']],
							['table', ['table']],
							['insert', ['kiwiLink', 'kiwiImage', 'hr', 'video']]
						];
						break;
					default:
						throw "Unsupported toolbar type.";
				}
			}
			var viewGroup = [];
			if(typeof fullScreen !== 'undefined' && fullScreen == 1){
				viewGroup.push('fullscreen');
			}
			if(typeof codeView !== 'undefined' && codeView == 1){
				viewGroup.push('codeview');
			}
			if(viewGroup.length > 0){
				toolbarButtons.push(['view', viewGroup]);
			}

			// apply toolbar
			toolbarButtons.push(['help', ['help']]);
			options.toolbar = toolbarButtons;
			
			$(item).addClass('hasEditor');
			$(item).summernote(options);
			
			// load text into editor
			var textareaId = $(item).data('for');
			var textareaText = $('#'+textareaId).val();
			
			if(textareaText.indexOf('<') !== -1){
				var $text = $('<div>');
				$text.html(textareaText);

				// replace link placeholders
				var result = '';
				$text.find('img').each(function(){
					if($(this).data('type') == 'internal'){
						var kiwiSrc = $(this).attr('src');
						var thumbUrl = $(this).data('thumbUrl');
						$(this).attr('src', thumbUrl);
						$(this).attr('data-save-url', kiwiSrc);
						$(this).removeAttr('data-thumb-url');
					}
				});
				$text.contents().each(function(){
					result += this.nodeType === 1 ? $(this).prop('outerHTML') : $(this).text();
				});

				$(item).summernote('code', result);
			}else{
				$(item).summernote('code', textareaText);
			}
			
		});
		
		// save text form editor on submit
		$('form.jsHasWysiwyg').submit(function(){
			$(this).find('.jsKiwiTextEditor.hasEditor').each(function(key, item){
				var textareaId = $(item).data('for');
				var textareaText = $(item).summernote('code');
				
				if(textareaText.indexOf('<') !== -1){
				
					var $text = $('<div>');
					$text.html(textareaText);

					// replace link placeholders
					var result = '';
					$text.find('img').each(function(){
						if($(this).data('type') == 'internal'){
							var translatedSrc = $(this).attr('src');
							var kiwiSrc = $(this).data('saveUrl');
							$(this).attr('src', kiwiSrc);
							$(this).attr('data-thumb-url', translatedSrc);
							$(this).removeAttr('data-save-url');
						}
					});
					$text.contents().each(function(){
						result += this.nodeType === 1 ? $(this).prop('outerHTML') : $(this).text();
					});

					$('#'+textareaId).val(result);
				}else{
					$('#'+textareaId).val(textareaText);
				}
			});
			
			return true;
		});
		

	});
})(jQuery, document);