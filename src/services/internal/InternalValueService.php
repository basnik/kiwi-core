<?php

namespace Kiwi\Services;

use Basnik\Db\Service;
use Kiwi\Entities\Value;

/**
 * Values
 *
 * @author basnik
 */
class InternalValueService extends Service {
	
	/** @var array Value cache */
	protected $valueCache = [];

	public function __construct(\Dibi\Connection $db) {
		parent::__construct($db, "kw_values", Value::class);
	}
	
	/**
	 * Gets value object by key. If value is not set, returns NULL.
	 * @param string $key Key of desired value.
	 * @return Value|NULL
	 */
	public function getByKey($key){
		if (!array_key_exists($key, $this->valueCache)) {
			$this->valueCache[$key] = $this->fetchObject(
				"SELECT * FROM %n WHERE ident = %s",
				$this->mainTable,
				$key
			);
		}

		return $this->valueCache[$key];
	}
	
	/**
	 * Invalidates value cache for page load.
	 */
	public function invalidateCache(){
		$this->valueCache = [];
	}
}

