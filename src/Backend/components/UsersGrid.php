<?php

namespace Kiwi\Be;

use \Doctrine\Common\Collections\Criteria;

class UsersGrid extends \Kiwi\Grid{

	/** @var type \Kiwi\Services\UserService */
	protected $users;

	function __construct(\Kiwi\Services\UserService $users){
		parent::__construct();

		$this->users = $users;
		
		// construct the grid itself
		
		$this->setRowPrimaryKey('id');
		$this->addCellsTemplate(__DIR__.'/../templates/Users/usersGrid.latte');

		$this->addColumn('email', 'E-mail')->enableSort();
		$this->addColumn('name', 'Jméno')->enableSort();
		$this->addColumn('password', 'Má heslo')->enableSort();
		$this->addColumn('blocked', 'Blokován')->enableSort();

		$this->setFilterFormFactory(function() {
			
			$form = new \Nette\Forms\Container();
			$form->addText('email');
			$form->addText('name');
			$form->addSelect('password', NULL, array(
				'' => 'Vše',
				'1' => 'Má',
				'0' => 'Nemá'
			));
			$form->addSelect('blocked', NULL, array(
				'' => 'Vše',
				'1' => 'Ano',
				'0' => 'Ne'
			));

			$form->addSubmit('filter', 'Filtrovat')->getControlPrototype()->class = 'btn btn-primary';
			$form->addSubmit('cancel', 'Zrušit filtr')->getControlPrototype()->class = 'btn';

			return $form;
		});
		
		$this->setDataSourceCallback([$this, "dataCallback"]);
	}

	public function dataCallback(array $filter, array $order=NULL, \Nette\Utils\Paginator $paginator=NULL) {
		
		// proccess filter
		$criteria = [];
		$orderBy = ["id" => "ASC"];
		if(!empty($filter)){
			foreach($filter as $column=>$val){
				if($column == 'name' || $column == 'email'){
					$criteria[] = ['%n LIKE %~like~', $column, $val];
				}else if($column == 'password'){
					if($val == 0){
						$criteria[] = ['%n IS NULL', $column];
					}else{
						$criteria[] = ['%n IS NOT NULL', $column];
					}
				}else{
					$criteria[] = ['%n = %s', $column, $val];
				}
			}
		}
		
		if($order !== NULL){
			$orderBy = [$order[0] => $order[1]];
			//$criteria->orderBy(array($order[0] => $order[1]));
		}

		return $this->users->getAllMatching($criteria, $orderBy);
	}

}
