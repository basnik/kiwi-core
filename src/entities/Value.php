<?php

namespace Kiwi\Entities;

use Basnik\Db\Entity;

/**
 * This reprfesents any value added to system. Can also be added by plugin.
 */
class Value extends Entity {
    
    /**
     * This can be used as a mark to retrieve data.
     */
    public $ident;
    
    /**
     * Any data added as value, json encoded.
     */
    public $data = [];


	/**
	 * Converts database row obtained as array to object.
	 * @param array $input
	 * @return User
	 */
	public static function load(array $input) {
		$value = new Value();
		$value->extractKeys($input, "id", "ident");
		$value->data = [];
		if (array_key_exists("data", $input)) {
			$value->data = json_decode($input["data"], TRUE) ?: [];
		}
		return $value;
	}

	/**
	 * Returns associative array with properties of this object - array should be used to store the model into database.
	 * @return array
	 */
	public function save() : array {
		$result = $this->exportProps( "id", "ident");
		$result["data"] = json_encode($this->data);
		return $result;
	}

}
