/**
 * Script for kiwi file chooser tool.
 * 
 * @author basnik
 */
;(function($, window, document, console){
	

	// namespace check
	if($.kiwi.be.fileChooser){
		throw new 'namespace kiwi.be.fileChooser is already used!';
	}
	
	// define our object
	$(document).ready(function(){
		
		$.kiwi.be.fileChooser = function(){
		
			var files = [];

			var self = {
				init: function(){
					$('.jsKiwiFileChooserInput:not(.ready)').each(function(){
						
						var $input = $(this);
						var $container = $(this).closest('.jsKiwiFileChooserCont');
						var $button = $container.find('.jsKiwiFileChooserTrigger:first');
						var $queue = $container.find('.jsKiwiFileChooserQueue:first');
						
						var files = [];

						$input.addClass('ready');

						$button.click(function(){

							var multiple = $input.data('multiple');
							var onlyimages = $input.data('onlyimages');
							var allowedtypes = $input.data('allowedtypes');
							
							if(!multiple){
								multiple = false;
							}
							if(!onlyimages){
								onlyimages = false;
							}
							if(!allowedtypes){
								allowedtypes = [];
							}
							
							$.kiwi.be.fileSelect({
								imagesOnly: onlyimages,
								multiple: multiple,
								allowedContentTypes: allowedtypes,
								selectedCallback: function(files){
									
									$.each(files, function(key, file){
										
										var $newFile = $queue.find('.jsKiwiFileChooserQueueItemSample:first').clone();
										$newFile.removeClass('jsKiwiFileChooserQueueItemSample hidden');
										var $link = $newFile.find('.jsKiwiFchImgLink');
										$link.attr('href', $link.data('link').replace('--ID--', file.id));

										if (onlyimages == true) {
											var $thumb = $newFile.find('img');
											$thumb.attr('src', $thumb.data('src').replace('--ID--', file.id));
										}

										if (multiple == true) {
											if(!$.inArray(file.id, files)){
												files.push(file.id);
												$queue.append($newFile);
											}
										} else {
											files = [file.id];
											$queue.find('.jsKiwiFChItem:not(.hidden)').remove();
											$queue.append($newFile);
										}

										// remove image/file button
										var $remButton = $newFile.find('.jsKiwiFChRemButton');
										self.initDeleteButton($remButton);
										$remButton.data('fileid', file.id);

										$input.val(files.join(','));
									});
								}
							});
						});
					});

					$('.jsKiwiFChRemButton:not(.ready)').each(function(){
						self.initDeleteButton($(this));
					});
				},
				initDeleteButton: function($remButton) {
					$remButton.addClass('ready');
					$remButton.click(function(){
						$.kiwi.be.fileSystemPickers.onDeleteButtonPress($(this), files);
					});
				}
			};

			self.init();

			return self; 
		}();
	});
	
}(jQuery, window, document, console));
