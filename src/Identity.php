<?php
namespace Kiwi;

/**
 * This class extends default indetity class to store the authentication method to be tested in future.
 *
 * @author basnik
 */
class Identity extends \Nette\Security\Identity{
	
	/** @var string Authenticator class name */
	protected $authenticator;

	/**
	 * 
	 * @param mixed $id identity ID
	 * @param Object $authenticator Object of authenticator (typically $this)
	 * @param mixed $roles
	 * @param array $data User data
	 */
	public function __construct($id, $authenticator, $roles = NULL, $data = NULL) {
		parent::__construct($id, $roles, $data);
		
		$this->authenticator = get_class($authenticator);
	}
	
	/**
	 * Returns name of used authenticator class as string.
	 * Suggested comparing method:
	 *		- before PHP 5.5: $identity->getAuthenticator() == MyAuthenticator::getReflection()->name
	 *		- after PHP 5.5: $identity->getAuthenticator() == MyAuthenticator::class
	 * 
	 * @return string
	 */
	public function getAuthenticator(){
		return $this->authenticator;
	}
	
}
