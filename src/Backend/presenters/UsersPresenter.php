<?php

namespace Kiwi\Be;

use Kiwi\Entities\User;
use Nette\Utils\Html;
use Nette\Application\UI\Form;
use Tomaj\Form\Renderer\BootstrapVerticalRenderer;

/**
 * Description of ContentPresenter
 *
 * @author basnik
 */
class UsersPresenter extends BasePresenter{
	
	protected $editedUserData;
	
	/** @var \Kiwi\Services\UserService */
	protected $userService;
	
	/** @var \Kiwi\Services\RoleService */
	protected $roleService;
	
	/** @var \Kiwi\Be\IUsersGridFactory */
	protected $usersGridFactory;
	
	public function startup() {
		parent::startup();
		
		if(!$this->user->isInRole('admin')){
			$this->redirect('Profile:');
		}
	}

	public function injectUserPresenter(\Kiwi\Services\UserService $users, \Kiwi\Services\RoleService $roleService, IUsersGridFactory $usersGridFactory){
		
		$this->userService = $users;
		$this->roleService = $roleService;
		$this->usersGridFactory = $usersGridFactory;
	}
	
	public function actionEdit($id=0){
		if(!empty($id)){
			$userData = $this->userService->getById($id);
			if(!empty($userData)){
				$this->editedUserData = $userData;
			}
		}
	}
	
	public function renderEdit($id=0){
		if(!empty($this->editedUserData)){
			$this->template->userData = $this->editedUserData;
		}
	}
	
	public function handleDelete($id){
		if(!empty($id)){
			$user = $this->userService->getById($id);
			if(!empty($user)){
				$this->userService->deleteById($id);
				$this->system->logActivity(sprintf("Uživatel %s (%s, %d) byl smazán", $user->email, $user->name, $user->id));
				$this->flashMessage('Uživatel '.$user->email.' smazán.', 'success');
			}
		}
		$this->redirect('Users:');
	}
	
	public function handleNewPass($id){
		if(!empty($id)){
			$user = $this->userService->getById($id);
			if(!empty($user)){
				
				$requestValidTill = $this->sendPassMail($user);
				$user->pwdChangeRequired = $requestValidTill;
				
				$this->userService->save($user);
				$this->system->logActivity(sprintf("Uživateli %s (%s, %d) odeslán odkaz na nastavení nového hesla.", $user->email, $user->name, $user->id));
				$this->flashMessage(sprintf('Uživateli %s odeslán e-mail s odkazem na nastavení nového hesla. Odkaz je platný do %s.', $user->email, $requestValidTill->format('j.n.Y H:i')), 'success');
			}
		}
		$this->redirect('Users:');
	}
	
	protected function createComponentEditForm(){
	
		$form = new Form();
		$form->setRenderer(new BootstrapVerticalRenderer());
		
		$form->addGroup('Profil uživatele');
		
		$form->addText('name', 'Jméno')
		    ->setRequired(true)
			->addRule(Form::MAX_LENGTH, 'Maximálně %d znaků.', 50);

		$form->addText('email', 'E-mail')
			->setRequired('E-mail musí být vyplněn.')
			->addRule(Form::EMAIL, 'E-mail musí mít správný formát')
			->addRule(Form::MAX_LENGTH, 'Maximálně %d znaků.', 50)
			->addRule([$this, 'validateEmailUniquity'], 'Takový e-mail již existuje.', !empty($this->editedUserData) ? $this->editedUserData->id : 0);
			
		
		if(empty($this->editedUserData)){
			$form->addCheckbox('sendPassMail','Odeslat e-mail s odkazem pro nastavení hesla')
			 	->setDefaultValue(TRUE);
		}
		$form->addCheckbox('block','Blokován');

		$rolesArray = array();
		foreach($this->roleService->getAll() as $role){
			
			$label = Html::el('abbr')->setText($role['name'])->addAttributes(array('title' => $role['description']));
			$rolesArray[$role['ident']] = $label;
		}
		
		if($this->roleService->getRoleCount() > 0){
			$form->addGroup('Přiřazené uživatelské role')
					->setOption('container', Html::el('fieldset')->class('role-list'));
			$form->addCheckboxList('roles', 'Zaškrtnuté role budou přiřazeny tomuto uživateli', $rolesArray);
		}

		$form->addGroup();
		$form->addSubmit('sendData', 'Uložit');
		$form->addSubmit('cancel', 'Zrušit')
				->setValidationScope([]); // validate nothing

		$form->addHidden('editId');
		
		$form->onSuccess[] = [$this, 'saveUser'];
		
		if(!empty($this->editedUserData)){
			$roleIds = array();
			foreach($this->editedUserData->roles as $role){
				if($this->roleService->roleExists($role)){
					$roleIds[] = $role;
				}
			}
			
			$form->setDefaults(array(
				'name' => $this->editedUserData->name,
				'email' =>  $this->editedUserData->email,
				'block' => (boolean) $this->editedUserData->blocked,
				'editId' =>  $this->editedUserData->id,
				'roles' => $roleIds
			));
		}
		
		return $form;
	}
	
	public function cancelForm(){
		$this->redirect('Users:');
	}

	protected function createComponentUserGrid(){
		return $this->usersGridFactory->create();
	}
	
	public function saveUser(Form $form){
		if(!$form['sendData']->isSubmittedBy()){
			$this->redirect('Users:');
			return;
		}
		$values = $form->getValues();
		
		if(!empty($values['editId'])){
			$user = $this->userService->getById($values['editId']);
		}else{
			$user = new User();
		}
		$oldUser = clone $user;
		
		$user->name = $values['name'];
		$user->email = $values['email'];
		$user->blocked = !empty($values['block']);
		
		// roles
		$user->roles = array_key_exists('roles', $values) ? $values['roles'] : array();

		$this->userService->save($user); // id is generated to $user->id
		
		// send password link via email if required
		if(!empty($values['sendPassMail'])){
			$passChangeTill = $this->sendPassMail($user);
			$user->pwdChangeRequired = $passChangeTill;
			$this->userService->save($user);
			$this->system->logActivity(sprintf("Uživateli %s (%s, %d) odeslán odkaz na nastavení nového hesla.", $user->email, $user->name, $user->id));
			$this->flashMessage(sprintf('Byl odeslán e-mail s odkazem pro nastavení hesla. Odkaz je platný do %s', $passChangeTill->format('j.n.Y H:i')), 'success');
		}

		if (!empty($values['editId'])) {
			$this->system->logActivity(sprintf("Uživatel %s (%s, %d) byl upraven. Změny: %s.", $user->email, $user->name, $user->id, $this->system->objDiff($oldUser, $user)));
		} else {
			$this->system->logActivity(sprintf("Uživatel %s (%s, %d) byl přidán.", $user->email, $user->name, $user->id));
		}

		$this->flashMessage('Změny byly uloženy.', 'success');
		$this->redirect('Users:');
	}
	
	// public to be accessible by callback
	public function validateEmailUniquity(\Nette\Forms\IControl $control, $userId){
		$userDb = $this->userService->getByEmail($control->getValue());
		if(!empty($userDb) && $userDb->id != $userId){
			return false;
		}
		return true;
	}
	
}
