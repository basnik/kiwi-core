<?php

namespace Kiwi\Be;

use Nette\Application\ForbiddenRequestException;
use \Nette\Application\Responses\JsonResponse;


/**
 * Description of ContentPresenter
 *
 * @author basnik
 */
class FilesPresenter extends BasePresenter{
	
	/** 
	 * @var int Id of current directory
	 * @persistent 
	 */
	public $id;
	
	/** @var Kiwi\Entities\Directory Current directory */
	protected $dir;

	/** @var \Kiwi\Services\FilesystemService */
	protected $filesystem;
	
	/* @var \Nette\Http\SessionSection */
	protected $session;
	
	/** @var int Filter for fileSelect */
	protected $imagesOnly;
	
	/** @var array Filter for fileSelect */
	protected $allowedContentTypes;
	
	/** @var IFileNameDialogFactory */
	protected $fileNameDialogFactory;
	
	/** @var IFileAddDialogFactory */
	protected $fileAddDialogFactory;
	
	/** @var IFilesystemGridFactory */
	protected $filesystemGridFactory;
	
	/** @var IFileselectGridFactory */
	protected $fileselectGridFactory;
	
	
	public function injectServices(\Kiwi\Services\FilesystemService $fs, \Nette\Http\Session $session, 
			IFileNameDialogFactory $fileNameFactory, IFileAddDialogFactory $fileAddFactory, 
			IFilesystemGridFactory $filesGridFactory, IFileselectGridFactory $fileselectGridFactory){
		
		$this->filesystem  = $fs;
		$this->session = $session->getSection(AFilesGrid::DIRSIZE_SESS_SECTION);
		$this->fileNameDialogFactory = $fileNameFactory;
		$this->fileAddDialogFactory = $fileAddFactory;
		$this->filesystemGridFactory = $filesGridFactory;
		$this->fileselectGridFactory = $fileselectGridFactory;
	}
	
	public function startup() {
		parent::startup();
		$this->setDirectory($this->id);
	}

	
	public function handleCount($countId){
		// get target directory
		$directory = $this->filesystem->getDirectoryById($countId);
		if($directory === NULL){
			throw new \Nette\InvalidArgumentException('Invalid directory id ('.$countId.').');
		}
		
		$this->session["$directory->id"] = $this->filesystem->getSizeOf($directory);
		$this['filesGrid']->redrawControl();
		
		if(!$this->isAjax()){
			$this->redirect('default');
		}
	}
	
	public function handleDeleteFile($deleteId){
		
		$file = $this->filesystem->getFileById($deleteId);
		if($file === NULL){
			throw new \Nette\InvalidArgumentException('Invalid file id ('.$deleteId.').');
		}

		if ($file->ownerId != $this->user->id && !$this->user->isInRole('admin')) {
			throw new ForbiddenRequestException("User not allowed to delete file $deleteId.");
		}
		
		$this->filesystem->deleteFile($file);

		$this->system->logActivity(sprintf('Soubor %s (%d) byl smazán ze složky %s.',
			$file->name, $deleteId, $this->getDirName($file->directoryId)));
		
		$this->flashMessage('Soubor '.$file->name.' byl smazán.','success');
		$this['filesGrid']->redrawControl();
		
		$this->redirect('default');
	}
	
	public function handleDeleteDirectory($deleteId){
		
		$directory = $this->filesystem->getDirectoryById($deleteId);
		if($directory === NULL){
			throw new \Nette\InvalidArgumentException('Invalid directory id ('.$deleteId.').');
		}
		
		if ($directory->ownerId != $this->user->id && !$this->user->isInRole('admin')) {
			throw new ForbiddenRequestException("User not allowed to delete directory $deleteId.");
		}
		
		$this->filesystem->deleteDirectory($directory);

		$this->system->logActivity(sprintf('Složka %s (%d) byla smazána ze složky %s.',
			$directory->name, $deleteId, $this->getDirName($directory->parentId)));

		$this->flashMessage('Složka '.$directory->name.' byla smazána.','success');
		$this['filesGrid']->redrawControl();
		
		$this->redirect('default');
	}
	
	public function actionGetAllowedThumbSizes(){
		if($this->isAjax()){
			$this->sendResponse(new JsonResponse($this->filesystem->getAllowedThumbnailSizes()));
		}else{
			throw new \Nette\InvalidStateException('This method can be called in ajax mode only.');
		}
	}
	
	public function actionGetPathTo($fileId){
		if(!is_numeric($fileId)){
			throw new \Nette\InvalidArgumentException('Id not provided or not numeric.');
		}
		
		if($this->isAjax()){
			$file = $this->filesystem->getFileById($fileId);
			$path = $this->getVirtualPathToFile($file);
			$this->sendResponse(new JsonResponse(array(
				'path' => $path .'/'. $file->name
			)));
		}else{
			throw new \Nette\InvalidStateException('This method can be called in ajax mode only.');
		}
	}
	
	/**
	 * Helps render file select javascript component content. Here mainly due form sends
	 */
	public function actionFileSelect($imagesOnly=0, array $allowedContentTypes=array()){
		$this->imagesOnly = $imagesOnly;
		$this->allowedContentTypes = $allowedContentTypes;
	}
	
	public function handleAddDir(){
		
		$this['fileNameDialog']->open();
		$this['fileNameDialog']->setTitle('Přidat složku');
		$this['fileNameDialog']->setInputLabel('Název složky');
		
		$this->redrawControl('dialogs');
	}
	
	public function handleRenameDirectory($renameId){
				
		$this['fileNameDialog']->open();
		$this['fileNameDialog']->setTitle('Přejmenovat složku');
		$this['fileNameDialog']->setInputLabel('Nový název složky');
		$this['fileNameDialog']->setEditedDirectory($renameId);
		
		$this->redrawControl('dialogs');
	}
	
	public function handleRenameFile($renameId){
		
		$this['fileNameDialog']->open();
		$this['fileNameDialog']->setTitle('Přejmenovat soubor');
		$this['fileNameDialog']->setInputLabel('Nový název souboru');
		$this['fileNameDialog']->setEditedFile($renameId);
		
		$this->redrawControl('dialogs');
	}
	
	public function handleUploadFiles(){

		$this['fileAddDialog']->open();
		
		$this->redrawControl('dialogs');
	}
	
	public function renderDefault() {
		
		$this->template->currentDir = $this->dir;
		$this->template->navPath = $this->getPathToCurrentDirectory();
		$this->template->dirSizes = $this->session;
		
		$this->template->kiwiBodyClasses[] = 'jsInitTooltip';
	}

	/**
	 * Renders file select javascript component content
	 */
	public function renderFileSelect($imagesOnly=0, array $allowedContentTypes=array()){
		
		$this->template->setFile(__DIR__.'/../templates/Files/fileSelect.latte');
		$navPath = $this->getPathToCurrentDirectory();
		$this->template->navPath = $navPath;
		
		// parse path as string to show it when the file is selected
		$path = array();
		foreach($navPath as $pathDir){
			$path[] = $pathDir->name;
		}
		$stringPath = implode('/', $path);

		// fetch files in directory and map it by id
		$dirContent = $this->filesystem->getDirectoryContent($this->dir->id ?? NULL, $this->imagesOnly, $this->allowedContentTypes);
		$files = array();
		foreach($dirContent as $item){
			
			if($item instanceof \Kiwi\Entities\File){
				$files[$item->id] = array(
					'id' => $item->id,
					'name' => $item->name,
					'contentType' => $item->contentType,
					'isImage' => $item->isImage,
					'size' => $item->size,
					'path' => $stringPath . '/' . $item->name
				);
			}
		}
		
		if($this->isAjax()){
			$this->sendResponse(new JsonResponse(array(
				'template' => (string) $this->template,
				'files' => $files
			)));
		}else{
			throw new \Nette\InvalidStateException('This method can be called in ajax mode only.');
		}
	}

	public function getDirName($dirId) {
		if ($dirId != NULL) {
			$dir = $this->filesystem->getDirectoryById($dirId);
			$dirName = sprintf("%s (%d)", $dir->name, $dir->id);
		} else {
			$dirName = "Domovská složka (parent)";
		}
		return $dirName;
	}

	
	/** COMPONENTS **/
	
	protected function createComponentFilesGrid(){
		$filesGrid = $this->filesystemGridFactory->create();
		$filesGrid->setDirectory($this->dir);
		return $filesGrid;
	}
	
	protected function createComponentFileselectGrid(){
		$filesGrid = $this->fileselectGridFactory->create();
		$filesGrid->setDirectory($this->dir);
		$filesGrid->setParameters($this->imagesOnly, $this->allowedContentTypes);
		return $filesGrid;
	}
	
	protected function createComponentFileNameDialog(){
		$dialog = $this->fileNameDialogFactory->create();
		$dialog->setDirectory($this->dir);
		$dialog->onSaveSuccess[] = function($message) use($dialog){
			$this->flashMessage($message, 'success');
			
			$this->redrawControl('fGridSnippet');
			$this->redrawControl('dialogs');
		};
		return $dialog;
	}
	
	protected function createComponentFileAddDialog(){
		$dialog = $this->fileAddDialogFactory->create();
		$dialog->setDirectory($this->dir);
		$dialog->onSaveSuccess[] = function() use($dialog){
			$this->flashMessage('Soubory byly přidány.', 'success');
			$dialog->close();
			
			$this->redrawControl('fGridSnippet');
			$this->redrawControl('dialogs');
		};
		$dialog->onFileError[] = function(){
			$this->redrawControl('fGridSnippet');
		};
		return $dialog;
	}
	
	/** PROTECTED HELPER METHODS **/
	
	protected function setDirectory($id){
		if($id !== NULL){
			$directory = $this->filesystem->getDirectoryById($id);
			if($directory === NULL){
				throw new \Nette\InvalidArgumentException('Directory does not exist ('.$id.').');
			}

			$this->id = $directory->id;
			$this->dir = $directory;
		}
	}
	
	protected function getPathToCurrentDirectory(){
		$navPath = [];
		if ($this->dir !== NULL) {
			$actualDir = $this->dir;
			
			// find path to show it in navigation
			do {
				$navPath[] = $actualDir;
				$actualDir = $actualDir->parentId !== NULL ? $this->filesystem->getDirectoryById($actualDir->parentId) : NULL;
			} while ($actualDir !== NULL);
		}
		
		return array_reverse($navPath);
	}
	
	/**
	 * Returns virtual path to a given file as string
	 * @return string
	 */
	protected function getVirtualPathToFile(\Kiwi\Entities\File $file){
		
		$parent = $file->getDirectory();
		
		$path = array();
		if($parent !== NULL){

			// find path to show it in navigation
			do{
				$path[] = $parent->getName();
				$parent = $parent->getParent();
			}while($parent !== NULL);
		}
		
		return implode('/', array_reverse($path));
	}
}
