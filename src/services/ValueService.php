<?php

namespace Kiwi\Services;

use Basnik\Db\BaseService;
use Kiwi\Entities\Value;

/**
 * Values
 *
 * @author basnik
 */
class ValueService {

    use \Nette\SmartObject;
	
	/** @var array Value cache */
	protected $valueCache = [];
	
	/** @var Internal service for values */
	protected $values;

	public function __construct(InternalValueService $values) {
		$this->values = $values;
	}

	/**
	 * Saves value to specified key, also refreshes the key in cache if exists.
	 * @param string $key
	 * @param mixed $value
	 * @return null
	 */
	public function set($key, $value) {
		$valueObj = $this->values->getByKey($key);
		if ($valueObj == NULL) {
			$valueObj = new Value();
			$valueObj->ident = $key;
			$valueObj->data = $value;
		} else {
			$valueObj->data = $value;
		}

		$this->values->save($valueObj);
		$this->valueCache[$key] = $valueObj;
	}

	/**
	 * Gets value by key. If value is not set, returns NULL.
	 * @param string $key Key of desired value.
	 * @return mixed|NULL
	 */
	public function get($key) {
		$valueObj = $this->values->getByKey($key);
		return $valueObj != NULL ? $valueObj->data : NULL;
	}
	
	/**
	 * Invalidates value cache for page load.
	 */
	public function invalidateCache(){
		$this->values->invalidateCache();
	}
}

