<?php

namespace Kiwi\Services;

use Basnik\Db\Service;
use Kiwi\Entities\Directory;

/**
 * Description of Kiwi
 *
 * @author basnik
 */
abstract class InternalFilesystemService extends Service {

	private $userTable = "kw_users";

	public function __construct(\Dibi\Connection $db, $mainTable, $objectClass) {
		parent::__construct($db, $mainTable, $objectClass);
	}

	public function getById($id) {
		return $this->fetchObject("SELECT e.*, u.email ownerEmail, u.name ownerName FROM %n e JOIN %n u ON(e.owner_id=u.id) WHERE e.id = %i",
			$this->mainTable, $this->userTable, $id);
	}

	public function getAll(array $order = ["id" => self::ASC], $limit = null, $offset = null) {
		return $this->fetchObjects("SELECT e.*, u.email ownerEmail, u.name ownerName FROM %n e JOIN %n u ON(e.owner_id=u.id) ORDER BY %by %lmt %ofs",
			$this->mainTable, $this->userTable, $order, $limit, $offset);
	}

	public function getAllMatching(array $andWhere, array $order = ["id" => self::ASC], $limit = null, $offset = null) {
		return $this->fetchObjects("SELECT e.*, u.email ownerEmail, u.name ownerName FROM %n e JOIN %n u ON(e.owner_id=u.id) WHERE %and ORDER BY %by %lmt %ofs",
			$this->mainTable, $this->userTable, $andWhere, $order, $limit, $offset);
	}
}
