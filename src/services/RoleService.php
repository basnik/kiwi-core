<?php
namespace Kiwi\Services;

use Kiwi\KiwiAuthorizator;

/**
 * This serves as a facade between user defined authorizator and rest of a system.
 *
 * @author basnik
 */
class RoleService {

    use \Nette\SmartObject;
	
	/** @var array Array of roles returned by authorizator */
	protected $roles;


	public function __construct(KiwiAuthorizator $auth=NULL) {
		
		$this->roles = ($auth === NULL ? $this->getDefaultRoles() : $auth->getRoles());
	}


	/**
	 * Gets one role by its ident.
	 * @param string $ident
	 * @return array|NULL
	 */
	public function getByIdent($ident) {
		return array_key_exists($ident, $this->roles) ? $this->roles[$ident] : NULL;
	}


	/**
	 * Checks if role exists.
	 * 
	 * @param string $ident
	 * @return boolean
	 */
	public function roleExists($ident){
		return array_key_exists($ident, $this->roles);
	}


	/**
	 * Gets all defined roles as array with keys: ident, name and description.
	 * 
	 * @return array
	 */
	public function getAll() {
		return $this->roles;
	}


	/**
	 * Returns number of roles defined in system.
	 * 
	 * @return int
	 */
	public function getRoleCount(){
		return count($this->roles);
	}


	public static function getDefaultRoles(){
		return array(
			'admin' => array(
				'ident' => 'admin',
				'name' => 'Administrátor',
				'description' => 'Administrátor může všude a všechno.'
			),
			'user' => array(
				'ident' => 'user',
				'name' => 'Uživatel',
				'description' => 'Může se přihlásit, ale nemůže spravovat uživatele a nemá přístup k nástrojům.'
			)
		);
	}
}
