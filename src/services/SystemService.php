<?php
namespace Kiwi\Services;

use Basnik\Db\Entity;
use Nette\InvalidArgumentException;
use Nette\Security\User;
use Nette\Utils\FileSystem;
use Tracy\Logger;

/**
 * Description of Kiwi
 *
 * @author basnik
 */
class SystemService {

    use \Nette\SmartObject;
	
	const VERSION = "2.0.0";
	
	/** @var string Name of the website */
	protected $webName;
	
	/** @var string Default address to be used as sender if no other sender is given */
	protected $defaultFromMail;
	
	/** @var array Array structure holding allowed image thumbnail sizes */
	protected $allowedThumbsizes;

	/** @var array Main backend layout latte file - path to it. Can be null. */
	protected $mainBeLayout;
	
	/** @var array Array contains database connection details */
	protected $dbConnDetails;
	
	/** @var string Full path to application directory where application source files should be placed */
	protected $appDirectory;
	
	/** @var string Full path to temporary directory of Nette framework */
	protected $tempDirectory;
	
	/** @var string Full path to vendor directory, where libraries are stored */
	protected $vendorDirectory;
	
	/** @var string Full path to web directory, where web static files are placed. */
	protected $webDirectory;
	
	/** @var string Full path to directory, where kiwi has root. */
	protected $kiwiDirectory;
	
	/** @var string Full path to directory, where kiwi stores uploaded files. */
	protected $filesDirectory;

	/** @var null Control to provide neccessary template data for main backend layout  */
	protected $mainBeLayoutControl = null;

	/** @var string Mode to fetch scripts and styles - can be either "dev" or "prod" */
	protected $resourcesMode;

	/** @var array List of roles which won't have backend profile settings (useful when users are authentized against some other service) */
	protected $noProfileForRoles;

	/** @var Logger Logger to log various activities in system */
	protected $activityLogger;

	/** @var User Current Nette user */
	protected $user;
	
	/**
	 * Constructor and DI
	 *
	 * @param string $appDir
	 * @param string $tempDir
	 * @param string $vendorDir
	 * @param string $wwwDir
	 * @param string $kiwiDir
	 * @param string $filesDir
	 * @param User $user
	 */
	public function __construct($appDir, $tempDir, $vendorDir, $wwwDir, $kiwiDir, $filesDir, User $user) {
		$this->appDirectory = $appDir;
		$this->tempDirectory = $tempDir;
		$this->vendorDirectory = $vendorDir;
		$this->webDirectory = $wwwDir;
		$this->kiwiDirectory = $kiwiDir;
		$this->filesDirectory = $filesDir;
		$this->user = $user;

		$activityLogDir = $this->appDirectory."/../log/activity";
		// creates the log directory if it does not exist
		FileSystem::createDir($activityLogDir);
		$this->activityLogger = new Logger($activityLogDir);
	}

	/** ---- GETTERS ------ **/
	
	/**
	 * Returns name of this website
	 * @return string
	 */
	public function getWebName() {
		return $this->webName;
	}

	/**
	 * Returns default address to be used as sender if no other sender is given
	 * @return type
	 */
	public function getDefaultFromMail() {
		return $this->defaultFromMail;
	}

	/**
	 * Returns array structure holding allowed image thumbnail sizes
	 * @return array
	 */
	public function getAllowedThumbsizes() {
		return $this->allowedThumbsizes;
	}
	
	/**
	 * Returns array structure holding database connection details
	 * @return array
	 */
	public function getDatabaseConnectionDetails() {
		return $this->dbConnDetails;
	}

	/**
	 * Returns path to main backend layout. Can be null.
	 * @return string
	 */
	public function getMainBeLayoutPath() {
		return $this->mainBeLayout;
	}

	/**
	 * Returns control to fill variables and controls to the main backend layout. Can be null.
	 * @return IBackendLayoutService
	 */
	public function getMainBeLayoutControl() {
		return $this->mainBeLayoutControl;
	}
	
	/**
	 * Returns full path to application directory where application source files should be placed
	 * @return string
	 */
	public function getAppDirectory() {
		return $this->appDirectory;
	}

	/**
	 * Returns full path to temporary directory of Nette framework
	 * @return string
	 */
	public function getTempDirectory() {
		return $this->tempDirectory;
	}

	/**
	 * Returns full path to vendor directory, where libraries are stored
	 * @return string
	 */
	public function getVendorDirectory() {
		return $this->vendorDirectory;
	}

	/**
	 * Returns full path to web directory, where web static files are placed
	 * @return string
	 */
	public function getWebDirectory() {
		return $this->webDirectory;
	}

	/**
	 * Returns full path to directory, where kiwi has root
	 * @return string
	 */
	public function getKiwiDirectory() {
		return $this->kiwiDirectory;
	}
	
	/**
	 * Returns full path to directory, where kiwi stores uploaded files
	 * @return string
	 */
	public function getFilesDirectory() {
		return $this->filesDirectory;
	}

	/**
	 * Returns list of roles which have profile section disabled
	 * @return array
	 */
	public function getNoProfileForRoles() {
		return $this->noProfileForRoles;
	}

	/**
	 * Returns resources mode for including scripts and styles.
	 * @return string
	 */
	public function getResourcesMode() {
		return $this->resourcesMode;
	}


	/** ---- SETTERS ------ **/

	public function setWebName($webName) {
		$this->webName = $webName;
	}

	public function setDefaultFromMail($defaultFromMail) {
		$this->defaultFromMail = $defaultFromMail;
	}

	public function setAllowedThumbsizes($allowedThumbsizes) {
		$this->allowedThumbsizes = $allowedThumbsizes;
	}
	
	public function setDatabase($connDetails) {
		$this->dbConnDetails = $connDetails;
	}

	public function setMainBeLayout($mainBeLayout) {
		$this->mainBeLayout = $mainBeLayout;
	}

	public function setBackendMainLayout($beMainLayoutControl) {
		$this->mainBeLayoutControl = $beMainLayoutControl;
	}

	public function setNoProfileForRoles($noProfileForRoles) {
		$this->noProfileForRoles = $noProfileForRoles;
	}

	public function setResourcesMode($resourcesMode) {
		if (!in_array($resourcesMode, ["dev", "prod"])) {
			throw new InvalidArgumentException("Resource mode must be one of 'dev' or 'prod'.");
		}
		$this->resourcesMode = $resourcesMode;
	}

	/** ----- ACTIVITY LOGGING ------- **/
	
	public function logActivity(string $message) {
		$userName = $this->user->identity->name." (".$this->user->getId().")";
		$this->activityLogger->log($userName.": ".$message);
	}

	public function objDiff($old, $new, $excludeProps = []) {
		if (get_class($old) != get_class($new)) {
			throw new \InvalidArgumentException("Only objects of same class can be compared. ".get_class($old)." and ".get_class($new)." given.");
		}

		$oldProps = get_object_vars($old);
		$newProps = get_object_vars($new);

		// credits https://stackoverflow.com/questions/19830585/why-array-diff-gives-array-to-string-conversion-error
		$checkDiff = function ($a, $b) use (&$checkDiff) {
			if (is_array($a) && is_array($b)) {
				return array_udiff_assoc($a, $b, $checkDiff);
			} else if ($a instanceof Entity && is_array($b)) {
				// to compare database fetched entities and lists of ids fetched from database
				return $a->id == $b["id"] ? 0 : ($a->id > $b["id"] ? 1 : -1);
			} else if (is_array($a) && $b instanceof Entity) {
				// to compare database fetched entities and lists of ids fetched from database
				return $a["id"] == $b->id ? 0 : ($a["id"] > $b->id ? 1 : -1);
			} else if (is_array($a)) {
				return 1;
			} else if (is_array($b)) {
				return -1;
			} else if (is_bool($a) || is_bool($b)) {
				$aInt = (int) $a;
				$bInt = (int) $b;
				return $a == $b ? 0 : ($a > $b ? 1 : -1);
			} else if ($a === $b) {
				return 0;
			} else if ($a instanceof Entity && $b instanceof Entity) {
				return $a->id == $b->id ? 0 : ($a->id > $b->id ? 1 : -1);
			} else if ($a instanceof \DateTime && $b instanceof \DateTime) {
				$aTime = $a->format("U");
				$bTime = $b->format("U");
				return $aTime == $bTime ? 0 : ($aTime > $bTime ? 1 : -1);
			} else {
				return $a > $b ? 1 : -1;
			}
		};

		$changedKeys = array_keys(array_udiff_assoc($oldProps, $newProps, $checkDiff));

		$result = [];
		foreach($changedKeys as $key) {
			if (in_array($key, $excludeProps)) {
				continue;
			}
			$result[] = $key.": ".$this->formatDiffProp($oldProps, $key)." => ".$this->formatDiffProp($newProps, $key);
		}

		return implode("; ", $result);
	}
		
	/** ----- HELPERS ------- **/

	private function formatDiffProp($valuesArray, $propName) {
		if (!isset($valuesArray[$propName])) {
			return "N/A";
		} else if (is_bool($valuesArray[$propName])) {
			return (int) $valuesArray[$propName];
		} else if ($valuesArray[$propName] instanceof Entity) {
			return $this->formatDiffEntity($valuesArray[$propName]);
		} else if (is_array($valuesArray[$propName])) {
			$vals = [];
			foreach ($valuesArray[$propName] as $val) {
				if ($val instanceof Entity) {
					$vals[] = $val->id;
				} else if (is_array($val) && array_key_exists("id", $val)) {
					$vals[] = $val["id"];
				} else {
					$vals[] = $val;
				}
			}
			return implode(",", $vals);
		}

		return (strlen($valuesArray[$propName]) > 103) ? substr($valuesArray[$propName],0,100).'...' : $valuesArray[$propName];
	}

	private function formatDiffEntity(Entity $entity) {
		return get_class($entity)."#".$entity->id;
	}
	
}

