<?php
namespace Kiwi\Be;

/**
 * Used for DI.
 * 
 * @internal
 * @author basnik
 */
interface IUsersGridFactory {
	
	/** @return \Kiwi\Be\UsersGrid */
	public function create();
}
