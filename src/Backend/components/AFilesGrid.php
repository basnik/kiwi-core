<?php

namespace Kiwi\Be;

use Nette\Utils\Strings;

abstract class AFilesGrid extends \Kiwi\Grid{
	
	const DIRSIZE_SESS_SECTION = 'fs-dirSizeCache';

	/** @var type \Kiwi\Services\FilesystemService */
	protected $filesystem;
	
	/** @var \Kiwi\Entities\Directory */
	protected $parentDir;
	
	/* @var \Nette\Http\SessionSection */
	protected $session;

	function __construct(\Kiwi\Services\FilesystemService $filesystem, \Nette\Http\Session $session){
		parent::__construct();

		$this->filesystem = $filesystem;
		$this->session = $session->getSection(AFilesGrid::DIRSIZE_SESS_SECTION);
		
		$this->setDataSourceCallback([$this, "dataCallback"]);
	}
	
	public function setDirectory(\Kiwi\Entities\Directory $parentDir=NULL){
		$this->parentDir = $parentDir;
	}
	
	public function dataCallback(array $filter, array $order=NULL, \Nette\Utils\Paginator $paginator=NULL) {
			
		$result = $this->getRawDataCollection();
		
		// normalize and complete data
		if($result == NULL){
			$result = array();
		}else{
			foreach ($result as $item){
				if($item instanceof \Kiwi\Entities\Directory && isset($this->session["$item->id"])){
					$item->size = $this->session["$item->id"];
				}
			}
		}
		
		// proccess filter
		if(!empty($filter)){
			$result = array_filter($result, function($item) use($filter){
				$include = TRUE;
				if(array_key_exists('name', $filter)){
					$include = Strings::contains(Strings::lower($item->name), Strings::lower($filter['name'])) ? TRUE : FALSE;
				}
				if(array_key_exists('ownerName', $filter)){
					$ownerNameMatches = Strings::contains(Strings::lower($item->ownerName), Strings::lower($filter['ownerName']));
					$ownerEmailMatches = Strings::contains(Strings::lower($item->ownerEmail), Strings::lower($filter['ownerName']));
					$include = ($ownerNameMatches || $ownerEmailMatches) ? TRUE : FALSE;
				}
				if($include == TRUE && array_key_exists('comparator', $filter) && array_key_exists('size', $filter) && array_key_exists('units', $filter)){
					$finalSize = $filter['size'];
					if($filter['units'] == 'k'){
						$finalSize *= 1024;
					}
					if($filter['units'] == 'M'){
						$finalSize *= 1048576; //1024*1024
					}
					
					if($filter['comparator'] == 'gt' && $item->size > $finalSize){
						$include = TRUE;
					}else if($filter['comparator'] == 'lt' && $item->size < $finalSize){
						$include = TRUE;
					}else{
						$include = FALSE;
					}
				}

				return $include;
			});
		}
		
		// proccess ordering
		if($order !== NULL){
			usort($result, function($a, $b) use($order){
				if($order[0] == 'name'){
					if($a instanceof \Kiwi\Entities\Directory && $b instanceof \Kiwi\Entities\Directory){
						$cmp = strcasecmp(Strings::toAscii($a->name), Strings::toAscii($b->name));
						return $order[1] == 'ASC' ? $cmp : -$cmp;
					}else if($a instanceof \Kiwi\Entities\Directory){
						return -1;
					}else if($b instanceof \Kiwi\Entities\Directory){
						return 1;
					}
				}else if($order[0] == 'size'){
					if($a instanceof \Kiwi\Entities\Directory && $b instanceof \Kiwi\Entities\Directory){
						$sizeCompare = $a->size >= $b->size ? 1 : -1;
						return $order[1] == 'ASC' ? $sizeCompare : -$sizeCompare;
					}else if($a instanceof \Kiwi\Entities\Directory){
						return -1;
					}else if($b instanceof \Kiwi\Entities\Directory){
						return 1;
					}
				}else if($order[0] == 'created'){
					if($a instanceof \Kiwi\Entities\Directory && $b instanceof \Kiwi\Entities\Directory){
						$compare = $a->created >= $b->created ? 1 : -1;
						return $order[1] == 'ASC' ? $compare : -$compare;
					}else if($a instanceof \Kiwi\Entities\Directory){
						return -1;
					}else if($b instanceof \Kiwi\Entities\Directory){
						return 1;
					}
				}else if($order[0] == 'ownerName'){
					if($a instanceof \Kiwi\Entities\Directory && $b instanceof \Kiwi\Entities\Directory){
						$aOwnerName = empty($a->ownerName) ? $a->ownerName : $a->ownerEmail;
						$bOwnerName = empty($b->ownerName) ? $b->ownerName : $b->ownerEmail;
						$compare = $aOwnerName >= $bOwnerName ? 1 : -1;
						return $order[1] == 'ASC' ? $compare : -$compare;
					}else if($a instanceof \Kiwi\Entities\Directory){
						return -1;
					}else if($b instanceof \Kiwi\Entities\Directory){
						return 1;
					}
				}
			});
		}
		
		return $result;
	}
	
	abstract protected function getRawDataCollection();
}
