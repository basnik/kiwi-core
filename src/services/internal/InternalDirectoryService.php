<?php

namespace Kiwi\Services;

use Basnik\Db\BaseService;
use Kiwi\Entities\Directory;

/**
 * Description of Kiwi
 *
 * @author basnik
 */
class InternalDirectoryService extends InternalFilesystemService {

	public function __construct(\Dibi\Connection $db) {
		parent::__construct($db, "kw_directories", Directory::class);
	}
}