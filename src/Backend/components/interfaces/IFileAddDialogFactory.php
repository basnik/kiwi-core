<?php
namespace Kiwi\Be;

/**
 * Used for DI.
 * 
 * @internal
 * @author basnik
 */
interface IFileAddDialogFactory {
	
	/** @return \Kiwi\Be\FileAddDialog */
	public function create();
}
