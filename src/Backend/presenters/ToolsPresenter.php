<?php

namespace Kiwi\Be;

use Nette\Utils\Finder;
use Nette\Utils\DateTime;
use Nette\Application\UI\Form;
use Tomaj\Form\Renderer\BootstrapVerticalRenderer;
use Nette\Utils\FileSystem;

/**
 * 
 *
 * @author basnik
 */
class ToolsPresenter extends BasePresenter{
	
	/** @var Kiwi\Services\SystemService */
	protected $system;
	
	public function __construct(\Kiwi\Services\SystemService $system) {
		parent::__construct();
		
		$this->system = $system;
	}

	public function startup() {
		parent::startup();
		
		if(!$this->user->isInRole('admin')){
			$this->redirect('Profile:');
		}
	}

	
	public function renderDefault(){
		$this->redirect('cache');
	}
	
	public function renderLogs($logFile=NULL){
		
		$logFiles = array();
		$currentContent = NULL;
		$logDirectoryPath = $this->system->getAppDirectory() . '/../log/';
		foreach (Finder::findFiles('*.log')->from($logDirectoryPath) as $file) {
			$logIdent = $this->getLogNameIdent($file, $logDirectoryPath);

			$logFiles[] = array(
				'name' => $logIdent,
				'path' => $file->getRealPath()
			);

			if($logFile == $logIdent){
				$currentContent = file_get_contents($file->getRealPath());
			}
		}
		
		if(count($logFiles) > 0 && $currentContent === NULL){
			$firstFile = reset($logFiles);
			$currentContent = file_get_contents($firstFile['path']);
			$logFile = $firstFile['name'];
		}
		
		$this->template->logFiles = $logFiles;
		$this->template->currentLogFile = $logFile;
		$this->template->currentContent = $currentContent;
	}

	private function getLogNameIdent($file, $logDirectoryPath) {
		$fullPathWithName = $file->getPath() . "/" . $file->getBaseName('.log');
		return str_replace($logDirectoryPath,"", $fullPathWithName);
	}
	
	public function renderExceptions($logFile=NULL){
		
		$logFiles = array();
		$name = NULL;
		foreach (Finder::findFiles('exception*.html')->from($this->system->getAppDirectory().'/../log/') as $file) {

			$fileNameExploded = explode('--', $file->getBaseName('.html'));
			$dt = DateTime::createFromFormat('Y-m-d H-i', $fileNameExploded[1].' '.$fileNameExploded[2]);
			
			$logFiles[] = array(
				'name' => $dt->format('j.n.Y H:i'),
				'fullName' => $file->getBaseName('.html')
			);
			
			if($logFile == $file->getBaseName('.html')){
				$name = $dt->format('j.n.Y H:i');
			}
		}
		
		usort($logFiles, function($a, $b){
			$da = DateTime::createFromFormat('j.n.Y H:i', $a['name']);
			$db = DateTime::createFromFormat('j.n.Y H:i', $b['name']);
			
			return $da < $db ? -1 : 1;
		});
		
		if($logFile === NULL){
			$firstFile = reset($logFiles);
			$logFile = $firstFile['fullName'];
			$name = $firstFile['name'];
		}

		$this->template->logFiles = $logFiles;
		$this->template->currentLogFile = $logFile;
		$this->template->currentName = $name;
	}
	
	public function renderExceptionContent($file){
		
		$filePath = $this->system->getAppDirectory().'/../log/'.$file.'.html';
		if(file_exists($filePath)){
			readfile($filePath);
		}else{
			echo 'File not found.';
			
		}
		
		$this->terminate();
	}
	
	public function renderPhpinfo(){
		ob_start();
		phpinfo();
		$pinfo = ob_get_contents();
		ob_end_clean();

		$this->template->phpinfo = preg_replace( '%^.*<body>(.*)</body>.*$%ms','$1',$pinfo);
	}
	
	public function renderComposer(){
		
		$composerContent = @file_get_contents($this->system->getAppDirectory().'/../composer.json'); // intentionally @
		$composerLockContent = @file_get_contents($this->system->getAppDirectory().'/../composer.lock'); // intentionally @
		
		$extensionsInstalled = array(
			'stable-packages' => array(),
			'dev-packages' => array(),
			'minimum-stability' => ''
		);
		if(!empty($composerLockContent)){
			$parsedContent = json_decode($composerLockContent, TRUE);
			if(!empty($parsedContent)){
				
				$extensionsInstalled['minimum-stability'] = $parsedContent['minimum-stability'];
				
				foreach($parsedContent['packages'] as $package){
					$extensionsInstalled['stable-packages'][] = array(
						'name' => $package['name'],
						'version' => $package['version'],
						'license' => array_key_exists('license', $package) ? implode(', ',$package['license']) : array('N/A'),
						'author' => array_key_exists('authors', $package) ? $this->parseAuthor($package) : 'N/A',
						'description' => array_key_exists('description', $package) ? $package['description'] :'N/A'
					);
				}
				
				foreach($parsedContent['packages-dev'] as $package){
					$extensionsInstalled['dev-packages'][] = array(
						'name' => $package['name'],
						'version' => $package['version'],
						'license' => array_key_exists('license', $package) ? implode(', ',$package['license']) : array('N/A'),
						'author' => array_key_exists('authors', $package) ? $this->parseAuthor($package) : 'N/A',
						'description' => array_key_exists('description', $package) ? $package['description'] :'N/A'
					);
				}
			}
		}
		
		$this->template->composerFileContents = !empty($composerContent) ? $composerContent : 'N/A';
		$this->template->composerLockFile = !empty($composerLockContent) ? $extensionsInstalled : FALSE;
	}
	
	
	protected function createComponentClearCacheForm(){
		
		$form = new Form();
		$form->setRenderer(new BootstrapVerticalRenderer());
		
		$form->addSubmit('clear_all', 'Vymazat vše');
		
		$form->onSuccess[] = [$this, "clearCacheSuccess"];
		
		return $form;
	}
	
	public function clearCacheSuccess(Form $form){
		
		$cacheDirPath = $this->system->getTempDirectory().'/cache/';
		
		if($form['clear_all']->isSubmittedBy()){
			
			try{
				foreach (Finder::find('*')->in($cacheDirPath) as $fileOrDir){
					FileSystem::delete($fileOrDir->getPathName());
				}

				FileSystem::delete($this->system->getTempDirectory()."/btfj.dat");

				$this->system->logActivity("Cache vyprázdněna.");

				$this->flashMessage('Cache byla vyprázděna.', 'success');
				$this->redirect('cache');

			}catch(\Nette\IOException $e){
				$this->flashMessage('Došlo k chybě při mazání cache. Detaily naleznete v zalogované výjimce.', 'error');
				\Tracy\Debugger::log($e, \Tracy\ILogger::EXCEPTION);
			}
		}
	}
	
	protected function parseAuthor($package){
		$author = array();
		if(array_key_exists('authors', $package)){
			
			if(count($package['authors']) > 2){
				$package['authors'] = array_slice($package['authors'], 0, 2);
				$append = ', a další ...';
			}
			
			foreach($package['authors'] as $authorArr){
				$authorText = '';
				if(array_key_exists('name', $authorArr)){
					$authorText = $authorArr['name'];
					if(array_key_exists('email', $authorArr)){
						$authorText .= '('.$authorArr['email'].')';
					}
				}else if(array_key_exists('email', $authorArr)){
					$authorText = $authorArr['email'];
				}
				$author[] = $authorText;
			}

		}
		
		return implode(', ', $author).(isset($append) ? $append : '');
	}

	
}
