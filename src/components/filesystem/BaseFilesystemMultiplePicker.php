<?php
namespace Kiwi;

/**
 * Base class for all filesystem multiple pickers, handles simmilar functionality of children classes.
 *
 * @author basnik
 */
class BaseFilesystemMultiplePicker extends BaseFilesystemPicker {
	
	
	/**
	 * Gets control from parent and perform common picker actions on it
	 * @return \Nette\Utils\Html
	 */
	public function getControl(): \Nette\Utils\Html {
		$control = parent::getControl();
		$control->value = $this->value;
		return $control;
	}
	
	/**
	 * Sets control's value.
	 * @param  string
	 * @return self
	 * @internal
	 */
	public function setValue($value)
	{
		if(is_array($value)){
			$value = implode(',', $value);
		}
		
		if ($value === NULL) {
			$value = array();
		} elseif (!is_scalar($value) && !method_exists($value, '__toString')) {
			throw new \Nette\InvalidArgumentException(sprintf("Value must be scalar or NULL, %s given in field '%s'.", gettype($value), $this->name));
		}
		$this->value = $value;
		return $this;
	}

	
	/**
	 * Gets control value
	 * 
	 * @return array
	 */
	public function getValue() {
		if(empty($this->value)){
			return array();
		} 
		
		$result = explode(',', $this->value);
		return array_map(function($val){ return intval($val); }, $result);
	}
}
