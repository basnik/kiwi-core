<?php
namespace Kiwi;

use Nette\Utils\Html;

/**
 * Selects multiple images from filesystem, returning their ids.
 * 
 * @author basnik
 */
class MultipleImagesPicker extends BaseFilesystemMultiplePicker{


	/**
	 * Gets control.
	 * 
	 * @return DatePicker
	 */
	public function getControl(): \Nette\Utils\Html {
		
		$parent = Html::el('div', array( 'class' => 'jsKiwiFileChooserCont'));
		
		$control = parent::getControl();
		$control->class('jsKiwiFileChooserInput hidden', TRUE);
		$control->addAttributes(array('data-onlyimages' => 1, 'data-multiple' => 1));
		
		if($this->isDisabled() || $this->getControlPrototype()->readonly){
			$button = Html::el('button', array('class' => 'btn btn-default', 'type' => 'button', 'disabled' => 'disabled'))->setText('Vyberte soubor');
		}else{
			$button = Html::el('button', array( 'class' => 'jsKiwiFileChooserTrigger btn btn-default', 'type' => 'button'))
				->setText('Vyberte soubor');
		}
		
		$queue = Html::el('div', array( 'class' => 'jsKiwiFileChooserQueue kiwiFChQueue'));
		$queue->addHtml($this->getQueueImageItem()); // sample for creating new files
		if(!empty($this->value)){
			foreach(explode(',', $this->value) as $fileId){
				$queue->addHtml($this->getQueueImageItem($fileId));
			}
		}
		
		$parent->addHtml($control);
		$parent->addHtml($button);
		$parent->addHtml($queue);
		
		return $parent;
	}

}
