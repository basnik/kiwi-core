<?php

namespace Kiwi;

use Nette\Application\UI\BadSignalException;
use Nette\Utils\Html;

/**
 * Description of MultipleUpload
 * 
 * @todo osetrit dlouha jmena souboru a obecne dlouhe vstupy
 *
 * @author basnik
 */
class MultipleUpload extends \Nette\Forms\Controls\BaseControl implements \Nette\Application\UI\ISignalReceiver{
	
	const UPLOAD_SIGNAL = 'upload';
	const UPLOAD_PARAM_NAME = 'mfuFiles';
	const SESSION_KEY = 'mfu';
	
	protected $uploadPath;
	
	// todo required, setAllowedTypes
	
	public function __construct($caption = NULL, $uploadPath = NULL) {
		parent::__construct($caption);

		if($uploadPath === NULL){
			throw new \Nette\InvalidArgumentException('No upload path specified for MultipleUpload.');
		}
		
		$this->uploadPath = $uploadPath;
	}
	
	public function getControl(): \Nette\Utils\Html {

		// main container to encapsulate the control
		$control = Html::el('div');
		$control->id = $this->getHtmlId();
		$control->addAttributes(array(
			'class' => 'jsMfuContainer fileUploadControl'
		));

		// upload button
		$submitButton = Html::el('div');
		$submitButton->class = 'fileUpload btn btn-default';
		
		$submitBtnText = Html::el('span');
		$submitBtnText->setText('Vyberte soubory k nahrání');
		
		// generate upload link - we need presenter and path from presenter to component
		$presenter = $this->lookup('\Nette\Application\IPresenter', TRUE);
		$componentPath = $this->lookupPath('\Nette\Application\IPresenter', TRUE);
		
		$fileInput = Html::el('input')->type('file');
		$fileInput->name = self::UPLOAD_PARAM_NAME;
		$fileInput->id = $this->getHtmlId().'-value';
		$fileInput->addAttributes(array(
			'data-url' => $presenter->link($componentPath.'-'.self::UPLOAD_SIGNAL.'!'),
			'data-param-name' => $componentPath.'-'.self::UPLOAD_PARAM_NAME,
			'multiple' => 'multiple',
			'class' => 'jsMfuControl upload'
		));
		
		$submitButton->addHtml($submitBtnText);
		$submitButton->addHtml($fileInput);
		$control->addHtml($submitButton);
		
		// upload queue
		$queue = Html::el('div');
		$queue->class = 'uploadQueue jsUploadQueue';
		
		// upload queue row
		$queueRow = Html::el('div');
		$queueRow->class = 'uplRow jsUploadQueueRow hide sample';
		
		$columns = array('name jsNameCell', 'size jsSizeCell', 'uplProg jsProgressCell', 'cancel jsCancelCell');
		foreach($columns as $column){
			$queueRowColumn = Html::el('div');
			$queueRowColumn->class = 'uplCell '.$column;
			
			if(substr_count($column, 'uplProg') > 0){
				$progress = Html::el('div');
				$progress->class = 'progress';
				$bar = Html::el('div');
				$bar->addAttributes(array(
					'class' => 'progress-bar',
					'role' => 'progressbar',
					'aria-valuenow' => '0',
					'aria-valuemin' => '0',
					'aria-valuemax' => '100',
					'style' => 'min-width: 2em;'
				));
				$bar->setText('0 %');
				$progress->addHtml($bar);
				
				$queueRowColumn->addHtml($progress);
			}else if(substr_count($column, 'cancel') > 0){
				$cancelBtn = Html::el('button');
				$cancelBtn->addAttributes(array(
					'type' => 'button',
					'class' => 'btn btn-default btn-xs jsCancelButton'
				));
				$cancelBtn->setText('Odebrat');
				$queueRowColumn->addHtml($cancelBtn);
			}
			
			$queueRow->addHtml($queueRowColumn);
		}
		
		$queue->addHtml($queueRow);
		$control->addHtml($queue);
		
		$clear = Html::el('div');
		$clear->class = 'clearfix';
		$control->addHtml($clear);
		
		// real form control to be submitted with form
		$formControl = Html::el('input');
		$formControl->addAttributes(array(
			'class' => 'jsMfuInput',
			'type' => 'hidden',
			'name' => $this->getHtmlName('value')
		));
		$control->addHtml($formControl);
		
		return $control;
	}
	
	public function getValue() {
		if(empty($this->value)){
			return NULL;
		}
		
		$presenter = $this->lookup('\Nette\Application\IPresenter', TRUE);
		$session = $presenter->getSession(self::SESSION_KEY);
		$sessionFiles = $session->offsetGet($this->getHtmlName('id'));
		if(!is_array($sessionFiles)){
			throw new \Nette\InvalidStateException('Incorrect file data format.');
		}
		
		$files = explode(';', $this->value);
		$result = array();
		foreach($files as $file){
			if(array_key_exists($file, $sessionFiles)){
				$result[] = $sessionFiles[$file];
			}
		}
		
		// reset session data
		$session->offsetSet(self::SESSION_KEY, array());
		
		return $result;
	}
	
	public function signalReceived(string $signal): void {
		if($signal == self::UPLOAD_SIGNAL){
			
			$presenter = $this->lookup('\Nette\Application\IPresenter', TRUE);
			$uploadControlName = $this->lookupPath('\Nette\Application\IPresenter', TRUE). '-' . self::UPLOAD_PARAM_NAME;
			$requestFiles = $presenter->getRequest()->getFiles();
			
			if(is_array($requestFiles) && array_key_exists($uploadControlName, $requestFiles)){
				
				$uploadedFile = isset($requestFiles[$uploadControlName]) ? $requestFiles[$uploadControlName] : NULL;
				if(empty($uploadedFile)){
					throw new \Nette\InvalidStateException('No file sent to upload.');
				}
				if(!$uploadedFile->isOk()){
					throw new UploadException('File upload error. More details in code.', $uploadedFile->getError());
				}
				
				$fileNameInTmp = basename($uploadedFile->getTemporaryFile());
				$newPath = $this->uploadPath.'/'.$fileNameInTmp;
				$uploadedFile->move($newPath);
				
				$file = new UploadedFile($fileNameInTmp, $uploadedFile->getName(), $newPath, intval($uploadedFile->getSize()));
				
				// Store file details in session, expiration until user closes browser
				$session = $presenter->getSession(self::SESSION_KEY);
				$session->setExpiration(0);
				$inputKey = $this->getHtmlName('id');
				$fileArray = $session->offsetExists($inputKey) ? $session->offsetGet($inputKey) : array();
				$fileArray[$file->getKey()] = $file;
				$session->offsetSet($inputKey, $fileArray);
	
				// send json response
				$response = new \Nette\Application\Responses\JsonResponse($file->toJSON());
				$presenter->sendResponse($response);
				
			}else{
				throw new \Nette\InvalidStateException('No file sent to upload.');
			}
			
		}else{
			throw new BadSignalException('There is no handler for signal \''.$signal.'\' in class '.get_class($this).'.');
		}
	}

}
