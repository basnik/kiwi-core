<?php
namespace Kiwi;

use Nette\InvalidArgumentException;
use Nette\Utils\Validators;
use Kiwi\Services\FilesystemService;
use Nette\Application\Routers\RouteList;
use Nette\Application\Routers\Route;

class KiwiExtension extends \Nette\DI\CompilerExtension {
	
	protected static $routeList = null;

	/** @var array Default values for system config */
	protected $defaults = array(
		'webName' => 'Kiwi !',
		'defaultFromMail' => 'noreply@dosllamas.cz',
		'allowedThumbsizes' => array('b' => array('100x100')),
		'noProfileForRoles' => [],
		'resourcesMode' => 'prod'
	);
	
	public function loadConfiguration() {

		$config = $this->getConfigWithDefaults();
		
		// check parameters and module initialization
		if(!Validators::isEmail($config['defaultFromMail'])){
			throw new ConfigurationException('Default "from" mail is not valid e-mail.', 0, E_WARNING);
		}
	}
	
	public function beforeCompile() {
		
		$builder = $this->getContainerBuilder();
		$config = $this->getConfigWithDefaults();

		// make sure thumbnail 100x100 is allowed for thumbnails to work correctly
		if (!array_key_exists("b", $config['allowedThumbsizes'])) {
			$config['allowedThumbsizes']["b"] = [];
		}
		if (!in_array("100x100", $config['allowedThumbsizes']["b"])) {
			array_push($config['allowedThumbsizes']["b"], "100x100");
		}

		// inject kiwi config parameters to system service
		$service = $builder->getDefinition($this->prefix('System'));
		$service->addSetup('setWebName', array($config['webName']));
		$service->addSetup('setDefaultFromMail', array($config['defaultFromMail']));
		$service->addSetup('setAllowedThumbsizes', array($config['allowedThumbsizes']));
		$service->addSetup('setDatabase', array($config['database']));
		$service->addSetup('setMainBeLayout', array($config['mainBeLayout']));
		$service->addSetup('setNoProfileForRoles', array($config['noProfileForRoles']));
		$service->addSetup('setResourcesMode', array($config['resourcesMode']));


		// define route for backend
		$builder->addDefinition($this->prefix('route.kiwi'))
			->setFactory('\Nette\Application\Routers\Route', array('kiwi/<presenter>/[<action>[/<id>]]', 'Be:Profile:'))
			->setAutowired(FALSE);
		
		// add our backend route to application router
		$routerService = $builder->getDefinition('router');
		$routerService->addSetup('\Kiwi\KiwiExtension::prependRoute($service, ?)', [$this->prefix('@route.kiwi')]);
		
	}
	
	/**
	 * @param  Nette\PhpGenerator\ClassType $class
	 * @return void
	 */
	public function afterCompile(\Nette\PhpGenerator\ClassType $class)
	{
		$config = $this->getConfigWithDefaults();
		$initialize = $class->getMethod('initialize');

		// check if main backend layout service is defined, if yes, link it to system service
		$initialize->addBody('$service_kwblp = $this->getByType("\Kiwi\Be\IBackendLayoutService", false);');
		$initialize->addBody('$service_kwss = $this->getByType("\Kiwi\Services\SystemService", true);');
		$initialize->addBody('if ($service_kwblp != null ) { $service_kwss->setBackendMainLayout($service_kwblp); }');

		$initialize->addBody('$context = $this;');
		$initialize->addBody('\Kiwi\KiwiExtension::registerFormExtensions($context->getByType(\'\Kiwi\Services\FilesystemService\'));');
	}
	
	/**
	 * Install Kiwi.
	 * 
	 * @param \Nette\Configurator $configurator
	 */
	public static function install(\Nette\Configurator $configurator){
		
		$configurator->addParameters(array(
			'kiwiDir' => __DIR__,
			'vendorDir' => __DIR__.'/../../..'
		));
		$configurator->addConfig(__DIR__ . '/kiwi.neon');
	}
	
	
	/**
	 * Prepends given route to given routelist.
	 * 
	 * @author Filip Prochazka
	 * @see https://github.com/Kdyby/Console/blob/master/src/Kdyby/Console/CliRouter.php#L124-144
	 * 
	 * @param \Nette\Application\IRouter $router
	 * @param \Nette\Application\Routers\Route $extensionRoute
	 * @throws \Nette\InvalidArgumentException
	 */
	public static function prependRoute(\Nette\Application\IRouter &$router, \Nette\Application\Routers\Route $extensionRoute)
	{
		if (!$router instanceof \Nette\Application\Routers\RouteList) {
			throw new \Nette\InvalidArgumentException(
				'If you want to use Kiwi then your main router '.
				'must be an instance of Nette\Application\Routers\RouteList'
			);
		}
		
		if(self::$routeList === null){
			self::$routeList = new RouteList('Kiwi');
			
			$router[] = self::$routeList;
			$lastKey = count($router) - 1;
			foreach ($router as $i => $route) {
				if ($i === $lastKey) {
					break;
				}

				$router[$i+1] = $route;
			}
			$router[0] = self::$routeList;
			
		}
		
		self::$routeList[] = $extensionRoute;
	}
	
	public static function registerFormExtensions(FilesystemService $filesystem){
		
		\Nette\Forms\Container::extensionMethod(
			'addWysiwyg', function (\Nette\Forms\Container $form, $name, $label = NULL, $toolbar = Wysiwyg::TOOLBAR_BASIC) {
				$component = new Wysiwyg($label);
				$component->setToolbar($toolbar);
				$component->setHeight(250);
				
				$form->addComponent($component, $name);
				$form->getElementPrototype()->class('jsHasWysiwyg', TRUE);
				
				return $component;
			}
		);
		
		\Nette\Forms\Container::extensionMethod(
			'addMultipleUpload', function (\Nette\Forms\Container $form, $name, $label = NULL, $uploadPath=NULL) {
				$component = new MultipleUpload($label, $uploadPath);
				$form->addComponent($component, $name);
				return $component;
			}
		);
		
		\Nette\Forms\Container::extensionMethod(
			'addDatePicker', function (\Nette\Forms\Container $form, $name, $label = NULL) {
				$component = new DatePicker($label);
				$form->addComponent($component, $name);
				return $component;
			}
		);
		
		\Nette\Forms\Container::extensionMethod(
			'addKiwiImage', function (\Nette\Forms\Container $form, $name, $label = NULL, array $allowedContentTypes=array()) use($filesystem) {
				$component = new SingleImagePicker($filesystem, $label, $allowedContentTypes);
				$form->addComponent($component, $name);
				return $component;
			}
		);
		
		\Nette\Forms\Container::extensionMethod(
			'addKiwiImages', function (\Nette\Forms\Container $form, $name, $label = NULL, array $allowedContentTypes=array()) use($filesystem) {
				$component = new MultipleImagesPicker($filesystem, $label, $allowedContentTypes);
				$form->addComponent($component, $name);
				return $component;
			}
		);
		
		\Nette\Forms\Container::extensionMethod(
			'addKiwiFile', function (\Nette\Forms\Container $form, $name, $label = NULL, array $allowedContentTypes=array()) use($filesystem) {
				$component = new SingleFilePicker($filesystem, $label, $allowedContentTypes);
				$form->addComponent($component, $name);
				return $component;
			}
		);
		
		\Nette\Forms\Container::extensionMethod(
			'addKiwiFiles', function (\Nette\Forms\Container $form, $name, $label = NULL, array $allowedContentTypes=array()) use($filesystem) {
				$component = new MultipleFilesPicker($filesystem, $label, $allowedContentTypes);
				$form->addComponent($component, $name);
				
				return $component;
			}
		);
	}

	protected function getConfigWithDefaults() {
	    return array_merge($this->defaults, $this->getConfig());
	}
}
