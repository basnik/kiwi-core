<?php
namespace Kiwi;

use Nette\Forms\Controls\BaseControl;
use Nette\Utils\Html;
use Kiwi\Services\FilesystemService;

/**
 * Base class for all filesystem pickers, handles simmilar functionality of children classes.
 *
 * @author basnik
 */
class BaseFilesystemPicker extends BaseControl {
	
	/** @var array */
	protected $allowedContentTypes = array();
	
	/** @var \Nette\Application\UI\Presenter */
	protected $presenter;
	
	/** @var FilesystemService */
	protected $filesystem;


	/**
	 * Construct new Picker and proccess options
	 * @param string $caption
	 * @param array $allowedContentTypes
	 */
	public function __construct(FilesystemService $filesystem, $caption = NULL, array $allowedContentTypes = array()) {
		parent::__construct($caption);

		$this->filesystem = $filesystem;
		$this->setAllowedContentTypes($allowedContentTypes);
	}
	
	/**
	 * Gets control from parent and perform common picker actions on it
	 * @return \Nette\Utils\Html
	 */
	public function getControl(): \Nette\Utils\Html {
		
		// first get presenter to be able to generate links to files
		$this->presenter = $this->lookup('Nette\Application\UI\Presenter', TRUE);
		
		$control = parent::getControl();
		
		// set properties as data attributes to parse in javascript
		$attrs = array();
		if(!empty($this->allowedContentTypes)){
			$attrs['data-allowedtypes'] = json_encode($this->allowedContentTypes);
		}
		$control->addAttributes($attrs);
		
		return $control;
	}
	
	/**
	 * Sets allowed content types for this picker.
	 * 
	 * @param array $allowedContentTypes
	 * @throws \Nette\InvalidArgumentException
	 */
	public function setAllowedContentTypes(array $allowedContentTypes = array()){
		if(!is_array($allowedContentTypes)){
			throw new \Nette\InvalidArgumentException('Allowed content types must be passed as array of strings.');
		}
		
		$this->allowedContentTypes = $allowedContentTypes;
	}
	
	/**
	 * Gets one image item for image pickers.
	 * 
	 * @return Html
	 */
	protected function getQueueImageItem($fileId=NULL){
		
		$queueItemSample = Html::el('div', array('class' => 'jsKiwiFileChooserQueueItemSample jsKiwiFChItem thumbnail'));
		$innerImgCont = Html::el('a', array('class'=> 'jsKiwiFchImgLink', 'target' => '_blank'));
		$innerImg = Html::el('img');
		$delButton = Html::el('button', array('class' => 'btn btn-sm btn-danger jsKiwiFChRemButton', 'type' => 'button'))->setHtml('Odebrat&nbsp;');
		
		if($fileId === NULL){
			$queueItemSample->class('hidden', TRUE);
			$innerImgCont->addAttributes(array('data-link' => $this->presenter->link(':Kiwi:File:get', '--ID--')));
			$innerImg->addAttributes(array('data-src' => $this->presenter->link(':Kiwi:File:image', '--ID--', 100, 100)));
			
		}else{
			$innerImgCont->addAttributes(array('href' => $this->presenter->link(':Kiwi:File:get', $fileId)));
			$innerImg->addAttributes(array('src' => $this->presenter->link(':Kiwi:File:image', $fileId, 100, 100)));
			$delButton->addAttributes(array('data-fileid' => $fileId));
		}
		
		$caption = Html::el('div', array('class' => 'caption'));
		$btnIcon = Html::el('i', array('class' => 'ico ico-delete'));

		$delButton->addHtml($btnIcon);
		$innerImgCont->addHtml($innerImg);
		$queueItemSample->addHtml($innerImgCont);
		if(!$this->isDisabled() && !$this->getControlPrototype()->readonly){
			$caption->addHtml($delButton);
			$queueItemSample->addHtml($caption);
		}

		return $queueItemSample;
	}
	
	/**
	 * Gets one file item for file pickers.
	 * 
	 * @return Html
	 */
	protected function getQueueFileItem($fileId=NULL){
		
		$queueItem = Html::el('div', array('class' => 'jsKiwiFileChooserQueueItemSample jsKiwiFChItem queueItem'));
		$delButton = Html::el('button', array('class' => 'btn btn-sm btn-danger jsKiwiFChRemButton', 'type' => 'button'))->setHtml('Odebrat&nbsp;');

		$icon = Html::el('i', array('class' => 'ico ico-file'));
		$queueItem->addHtml($icon);
		
		// file link has file name as its text, inserted via javascript
		$innerImgCont = Html::el('a', array('class'=> 'jsKiwiFchImgLink fileLink', 'target' => '_blank'));
		if($fileId === NULL){
			$innerImgCont->addAttributes(array('data-link' => $this->presenter->link(':Kiwi:File:download','--ID--')));
			$queueItem->class('hidden', TRUE);
			
		}else{
			
			// does the file exist? load it
			$file = $this->filesystem->getFileById($fileId);
			if(empty($file)){
				// if the file does not exist, log error and silently end
				\Tracy\Debugger::log(sprintf('File not found for edit. File id is %d.', $fileId), \Tracy\Debugger::ERROR);
				return ''; // empty string will be added to page html
			}
			
			$innerImgCont->addAttributes(array('href' => $this->presenter->link(':Kiwi:File:download', $fileId)));
			$innerImgCont->setText($file->name);
			$delButton->addAttributes(array('data-fileid' => $fileId));
		}
		
		$btnIcon = Html::el('i', array('class' => 'ico ico-delete'));

		$delButton->addHtml($btnIcon);
		$queueItem->addHtml($innerImgCont);
		if(!$this->isDisabled() && !$this->getControlPrototype()->readonly){
			$queueItem->addHtml($delButton);
		}
		
		return $queueItem;
	}
	
}
